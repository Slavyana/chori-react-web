import ClientService from "../services/client-service";

export const getClient = (id) => {
    return ClientService.getClient(id).then(
        (response) => {
            return response
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            return message
        }
    );
};