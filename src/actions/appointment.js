import AppointmentService from "../services/appointment-service";

export const postAppointment = (businessId, name, description, price, duration) => {
    return AppointmentService.postAppointment(businessId, name, description, price, duration).then(
        (response) => {
            return response
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            return message
        }
    );
};