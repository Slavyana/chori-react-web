import EmployeeService from "../services/employee-service";

export const getEmployee = (id) => {
    return EmployeeService.getEmployee(id).then(
        (response) => {
            return response
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            return message
        }
    );
};