import BusinessService from "../services/business-service";

export const getBusiness = (id) => {
    return BusinessService.getBusiness(id).then(
        (response) => {
            return response
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            return message
        }
    );
};

export const getBusinessServices = (id) => {
    return BusinessService.getBusinessServices(id).then(
        (response) => {
            return response
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            return message
        }
    );
};

export const getBusinessEmployees = (id) => {
    return BusinessService.getBusinessEmployees(id).then(
        (response) => {
            return response
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            return message
        }
    );
};


export const getBusinessProducts = (id) => {
    return BusinessService.getBusinessProducts(id).then(
        (response) => {
            return response
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            return message
        }
    );
};

export const getBusinessPosts = (id) => {
    return BusinessService.getBusinessPosts(id).then(
        (response) => {
            return response
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            return message
        }
    );
};