import ServiceService from "../services/service-service";

export const postService = (businessId, name, description, price, currency, duration) => {
    return ServiceService.postService(businessId, name, description, price, currency, duration).then(
        (response) => {
            return response
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            return message
        }
    );
};