import React, { useEffect, useState} from 'react';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';

import {useSelector} from "react-redux";
import {Page} from "./components/components";
import Select from "react-select";
import {getBusiness, getBusinessServices} from "./actions/business";
import Box from "@material-ui/core/Box";
import Rating from "@material-ui/lab/Rating";

let options = []

export function ReviewEdit(props) {

    let handleCancel = (event) => {
        props.history.push('/client-profile');
    }

    const { user: currentUser } = useSelector((state) => state.auth);

    let emptyReview = {
        author: currentUser,
        rating: 5,
        content: '',
        isAnonymous: false,
        business: {},
        service: {},
        employee: {}
    };

    const [isLoaded, setIsLoaded] = useState(false);
    const id = props.match.params.id;
    const businessId = props.match.params.businessId;

    const [review, setReview] = useState(emptyReview);
    const [error, setError] = useState(null);
    const [selectedOption, setSelectedOption] = useState([]);
    const [services, setServices] = useState([]);


    useEffect(() => {
        options = []
        if(businessId) {
            getBusinessServices(businessId)
                .then((response) => {
                    setServices(response);
                });

            getBusiness(businessId)
                .then((response) => {
                    let changedReview = {...review};
                    changedReview["business"] = response;
                    setReview(changedReview);

                });
        }

    }, [businessId])

    useEffect(() => {
        services.map((service => options.push({value: service.id, label: service.name})))
        console.log(options)

    }, [services])



    useEffect(() => {
        let changedReview = {...review};
        changedReview["service"] = selectedOption;
        setReview(changedReview);
    },[selectedOption])



    useEffect(() => {
        if(id) {
            fetch(`http://localhost:8080/api/reviews/${id}`)
                .then(res => res.json())
                .then(
                    (result) => {
                        setIsLoaded(true);
                        setReview(result);
                    },
                    (error) => {
                        setIsLoaded(true);
                        setError(error);
                    }
                )
        }

    }, [])

    const title = id ? <h2>Edit review</h2> : <h2>Add a review</h2>;


    let handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        console.log(event);
        let changedReview = {...review};
        changedReview[name] = value;
        setReview(changedReview);
    }

    let handleSubmit = (event) => {
        event.preventDefault();

        fetch('http://localhost:8080/api/reviews' ,{
            method: (id) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(review),
        }).then(() => props.history.push('/profile-' + review.business.username));
    }


    useEffect(() => {
        let changedReview = {...review};
        changedReview["author"] = {...currentUser, roles: null};
        setReview(changedReview);
        console.log(changedReview);

    }, [])


    return (
        <Page>
            <Container className={"white-background padding"}>
                <Form className={"new-form"} onSubmit={handleSubmit}>
                    {title}

                    <div className={"white-background"}>
                        <FormGroup>
                            <Label for="description">Rating</Label>
                            <Box component="fieldset" borderColor="transparent">
                                <Rating
                                    name="simple-controlled"
                                    value={review.rating}
                                    onChange={(event, newValue) => {
                                        let changedReview = {...review};
                                        changedReview["rating"] = newValue;
                                        setReview(changedReview);
                                    }}
                                />
                            </Box>
                        </FormGroup>


                        <FormGroup>
                            <Label for="description">Review</Label>
                            <Input type="textarea" name="content" id="content" value={review.content || ''}
                                   onChange={handleChange} autoComplete="content"/>
                        </FormGroup>


                        <FormGroup>
                            <Label for="services">Services</Label>
                            <Select
                                name=""
                                options={options}
                                defaultValue={selectedOption}
                                onChange={setSelectedOption}
                                className="basic-multi-select"
                                classNamePrefix="select"
                            />
                        </FormGroup>

                        <div>
                            <input className={"checkbox"} type="checkbox" id="isAnonymous" name="isAnonymous" onClick={(e)=> {

                                let changedReview = {...review};
                                changedReview["isAnonymous"] = !review.isAnonymous;
                                setReview(changedReview);
                                console.log(changedReview);

                            }}/>
                                <label htmlFor="horns">Anonymous</label>
                        </div>

                        <div>
                            <button className={"button-primary"} type={"submit"}>Save</button>
                            <button className={"button-secondary"} onClick={handleCancel}>Cancel</button>
                            <br/>
                        </div>
                    </div>

                </Form>
            </Container>
        </Page>);
}

