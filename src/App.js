import React, {useEffect} from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {useDispatch, useSelector} from "react-redux";
import {clearMessage} from "./actions/message";
import {history} from "./helpers/history";
import AddEmployee from "./components/AddEmployee";
import AddService from "./components/AddService";
import {EmployeeEdit} from "./EmployeeEdit";
import {ServiceEdit} from "./ServiceEdit";
import {BusinessEdit} from "./BusinessEdit";
import ClientProfile from "./components/ClientProfile";
import UploadFiles from "./components/UploadFile";
import {BookAppointment, Scheduler} from "./components/Scheduler";
import {ClientEdit, ProfileEdit} from "./updated_pages/EditProfile";
import {Post} from "./updated_pages/Posts";
import {NewProfile} from "./updated_pages/Profile";
import {PostEdit} from "./components/PostEdit";
import {ProductEdit} from "./components/ProductEdit";
import {CommentEdit} from "./components/CommentEdit";
import {ReviewEdit} from "./ReviewsEdit";
import {PrivacyPolicy, TermsAndConditions} from "./TermsAndConditions";
import {CookiesProvider} from 'react-cookie';
import {HomePage} from "./pages/homePage";
import {ClientScheduler} from "./components/ClientScheduler";
import {BookingPage} from "./pages/bookingPage";

function App() {
    const {user: currentUser} = useSelector((state) => state.auth);
    const dispatch = useDispatch();
//
    useEffect(() => {

        history.listen((location) => {
            dispatch(clearMessage()); // clear message when changing location
        });
    }, [dispatch]);

    return (
        <CookiesProvider>
            <Router history={history}>

                <Switch>
                    <Route path="/profile/employees/:id" component={EmployeeEdit}/>
                    <Route path="/profile/services/:id" component={ServiceEdit}/>
                    <Route path="/profile/business/:id" component={BusinessEdit}/>
                    <Route path="/profile/client/:id" component={ClientEdit}/>
                    <Route exact path="/posts/:id" component={PostEdit}/>
                    <Route exact path="/posts/:postId/comment/:commentId" component={CommentEdit}/>
                    <Route path="/:businessId/reviews/add" component={ReviewEdit}/>

                    <Route path="/profile-:username" component={NewProfile}/>
                    <Route exact path="/profile" component={NewProfile}/>
                    <Route path="/client-profile-:username" component={ClientProfile}/>

                    <Route path="/employees/" component={AddEmployee}/>
                    <Route exact path="/services" component={AddService}/>

                    <Route exact path="/appointments" component={Scheduler}/>
                    <Route exact path="/client-appointments" component={ClientScheduler}/>

                    <Route path="/:businessId/appointments/add/:serviceId" component={BookAppointment}/>
                    <Route path="/:businessId/appointments/add" component={BookAppointment}/>

                    <Route exact path="/client-profile" component={ClientProfile}/>
                    <Route exact path="/upload" component={UploadFiles}/>
                    <Route exact path="/schedule" component={Scheduler}/>
                    <Route path="/posts/:id" component={PostEdit}/>
                    <Route path="/posts" component={PostEdit}/>

                    <Route exact path="/products" component={ProductEdit}/>
                    <Route path="/products/:id" component={ProductEdit}/>

                    <Route path="/new-profile" component={NewProfile}/>
                    <Route path="/discover" component={Post}/>

                    <Route path="/search" component={BookingPage}/>

                    <Route exact path="/terms" component={TermsAndConditions}/>
                    <Route exact path="/policy" component={PrivacyPolicy}/>

                    <Route path="/">
                        <HomePage/>
                    </Route>

                </Switch>
            </Router>
        </CookiesProvider>
    );
}

export default App;


