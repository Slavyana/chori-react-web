import React, {useEffect, useState} from 'react';

import {ReactSearchAutocomplete} from 'react-search-autocomplete'
import {Button, ButtonGroup} from "react-bootstrap";
import {Page} from "./components";


const SearchPage = (props) => {
    const [input, setInput] = useState('');
    const [countryListDefault, setCountryListDefault] = useState();
    const [searchList, setSearchList] = useState([]);
    const [businessSearchList, setBusinessSearchList] = useState([]);
    const [clientSearchList, setClientSearchList] = useState([]);


    const changeSearch = (e) => {
        console.log(e)
        fetch('http://localhost:8080/api/businesses?search=' + e)
            .then(response => response.json())
            .then(data => {
                let list = data.map(d => {
                    return {...d, "name": `${d.username}, ${d.name} (${d.region})`}
                });
                setBusinessSearchList(list)
            });

        fetch('http://localhost:8080/api/clients?search=' + e)
            .then(response => response.json())
            .then(data => {
                let list = data.map(d => {
                    return {...d, "name": `${d.username}, ${d.firstName} ${d.lastName}`}
                });
                setClientSearchList(list)
            });

    }


    const fetchData = async () => {
        return await fetch('https://restcountries.eu/rest/v2/all')
            .then(response => response.json())
            .then(data => {
                // setCountryList(data)

            });
    }

    const updateInput = async (input) => {
        const filtered = countryListDefault.filter(country => {
            return country.name.toLowerCase().includes(input.toLowerCase())
        })
        setInput(input);
        // setCountryList(filtered);
    }

    useEffect(() => {
        fetchData();
    }, []);

    const items = [
        {
            id: 0,
            name: 'Cobol'
        },
        {
            id: 1,
            name: 'JavaScript'
        },
        {
            id: 2,
            name: 'Basic'
        },
        {
            id: 3,
            name: 'PHP'
        },
        {
            id: 4,
            name: 'Java'
        }
    ]

    const handleOnSearch = (string, results) => {
        // onSearch will have as the first callback parameter
        // the string searched and for the second the results. s
        console.log(string, results)
        changeSearch(string)
    }

    const handleOnHover = (result) => {
        // the item hovered
        console.log(result)
    }

    const handleOnSelect = (item) => {
        // the item selected
        console.log(item)
        props.history.push("profile/" + item.username );
    }

    const handleOnFocus = () => {
        console.log('Focused')

    }


    const [showUserForm, setShowUserForm] = useState(true)
    const [userClassName, setUserClassName] = useState("account-select-btn active")
    const [businessClassName, setBusinessClassName] = useState("account-select-btn")

    const handleUserTab = () => {
        // console.log("moooo")
        setUserClassName("account-select-btn active")
        setBusinessClassName("account-select-btn")
        setShowUserForm(true)
    }

    const handleBusinessTab = () => {
        setUserClassName("account-select-btn")
        setBusinessClassName("account-select-btn active")
        setShowUserForm(false)
    }

    return (
        <Page>
            <div>
                <div className={"white-background"}>


                    <ButtonGroup className={"account-select-btn-group"} aria-label="Basic example">
                        <Button className={userClassName} onClick={handleUserTab}>Search users</Button>
                        <Button className={businessClassName} onClick={handleBusinessTab}>Search businesses</Button>
                    </ButtonGroup>


                </div>

                <ReactSearchAutocomplete
                    items={(showUserForm && clientSearchList) || businessSearchList}
                    fuseOptions={{keys: ["firstName", "lastName", "username", "name"]}}
                    resultStringKeyName="name"
                    onSearch={handleOnSearch}
                    onHover={handleOnHover}
                    onSelect={handleOnSelect}
                    onFocus={handleOnFocus}
                    autoFocus
                />
            </div>
        </Page>

    )

    // return (
    //     <div>
    //         <div className="wrapper">
    //             <div className="search-input">
    //                 <a href="" target="_blank" hidden></a>
    //
    //                 <input type="text" placeholder="Type to search.."/>
    //                     <div className="autocom-box">
    //                     </div>
    //                     <div className="icon">Search</div>
    //             </div>
    //
    //
    //         </div>
    //
    //         <SearchField
    //             placeholder='Search...'
    //             onChange={changeSearch}/>
    //
    //         <div className={"white-background"}>
    //             <h6>Businesses</h6>
    //             {businessSearchList.map(item => <li>{item.username}, {item.name}</li>)}
    //         </div>
    //         <div>
    //             <h6>Clients</h6>
    //             {clientSearchList.map(item => <li>{item.username}, {item.firstName} {item.lastName}</li>)}
    //         </div>
    //     </div>
    // );
}


export default SearchPage
