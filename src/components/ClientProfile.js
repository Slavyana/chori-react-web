import React, {useEffect, useState} from "react";
import {Link, Redirect} from 'react-router-dom';
import {useSelector} from "react-redux";
import {CustomModal, Page} from "./components";
import {getClient} from "../actions/client";
import User2 from "../updated_pages/img/user2.jpg";
import {Container} from "react-bootstrap";
import photo from "../static/images/photo-icon.png";
import {Button} from "reactstrap";
import {getBusiness, getBusinessServices} from "../actions/business";

const ClientProfile = (props) => {
    const {user: currentUser} = useSelector((state) => state.auth);
    const [client, setClient] = useState({});
    const [posts, setPosts] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [selectedPost, setSelectedPost] = useState(null);
    const [services, setServices] = useState([]);
    const username = props.match.params.username;
    console.log(username)

    useEffect(() => {
        if (!username) {
            try {
                fetch(`http://localhost:8080/api/clients/${currentUser.id}/posts`)
                    .then(res => res.json())
                    .then(
                        (result) => {
                            setPosts(result);
                            console.log(posts)
                        })
            } catch (e) {
                props.history.push("/profile")
            }

        } else {
            fetch('http://localhost:8080/api/clients?search=' + username)
                .then(response => response.json())
                .then(data => {
                    console.log(data)
                    setClient(data[0])
                });
        }
    }, []);


    useEffect(() => {
        getClient(currentUser.client.id)
            .then((response) => {
                setClient(response);
            });


    }, [])



    useEffect(() => {
        fetch(`http://localhost:8080/api/clients/${currentUser.id}/posts`)
            .then(res => res.json())
            .then(
                (result) => {
                    setPosts(result);
                    console.log(posts)
                },
                (error) => {
                }
            )
    }, [])

    if (!currentUser) {
        return <Redirect to="/"/>;
    }



    const handleCloseModal = () => setShowModal(false);



    let handlePostClick = (post) => {
        setSelectedPost(<div className="row">
            <div className=" offset-sm-3">
                <div className="post-block">
                    <div className="d-flex justify-content-between">
                        <div className="d-flex mb-3">
                            <div className="mr-2">
                            </div>
                            <div>
                                <h5 className="mb-0"><a href="#!" className="text-dark">{post.business.username}</a>
                                </h5>
                            </div>
                        </div>
                        <div className="post-block__user-options">
                            <a href="#!" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                <i className="fa fa-ellipsis-v" aria-hidden="true"></i>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right"
                                 aria-labelledby="triggerId">
                                <a className="dropdown-item text-dark" href="#!"><i
                                    className="fa fa-pencil mr-1"></i>Edit</a>
                                <a className="dropdown-item text-danger" href="#!"><i
                                    className="fa fa-trash mr-1"></i>Delete</a>
                            </div>
                        </div>
                    </div>
                    <div className="post-block__content mb-2">
                        <p>{post.description}</p>
                        <img src={post.image.url} alt="Content img"/>
                    </div>
                    <div className="mb-3">
                        <div className="d-flex justify-content-between mb-2">
                            <div className="d-flex">
                                <a href="#!" className="text-danger mr-2"><span>Like</span></a>
                                <a href="#!" className="text-dark mr-2"><span>Unlike</span></a>
                            </div>
                        </div>
                        {post.likes.length > 0 && <p className="mb-0">Liked by <a href="#!"
                                                                                  className="text-muted font-weight-bold">{post.likes[0].username}</a> &
                            <a href="#!" className="text-muted font-weight-bold"> {post.likes.length - 1} other</a>
                        </p>}

                    </div>
                    <hr/>
                    <div className="post-block__comments">
                        <div className="input-group mb-3">
                            <input type="text" className="form-control"
                                   placeholder="Add your comment"/>
                            <div className="input-group-append">
                                <button className="btn btn-primary" type="button"
                                        id="button-addon2"><i className="fa fa-paper-plane"></i>
                                </button>
                            </div>
                        </div>

                        {/* scroll */}

                        <div className="comment-view-box mb-3">
                            <div className="d-flex mb-2">
                                <img src={User2} alt="User img"
                                     className="author-img author-img--small mr-2"/>
                                <div>
                                    <h6 className="mb-1"><a href="#!" className="text-dark">John
                                        doe</a></h6>
                                    <p className="mb-1">Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit.</p>
                                    <div className="d-flex">
                                        <a href="#!" className="text-dark mr-2"><span><i
                                            className="fa fa-heart-o"></i></span></a>
                                        <a href="#!"
                                           className="text-dark mr-2"><span>Like</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr/>
                        <a href="#!" className="text-dark">View More comments <span
                            className="font-weight-bold">({post.comments.length})</span></a>
                    </div>
                </div>
            </div>
        </div>)
        setShowModal(true)
    }

    const postsList = posts.map(post => {
        return <div className="u-effect-fade u-gallery-item" onClick={() => handlePostClick(post)}>
            <div className="u-back-slide">
                {post.image && <img className="u-back-image u-expanded" src={post.image.url}/>}
            </div>
            <div className="u-over-slide u-shading u-over-slide-1">
                <h3 className="u-gallery-heading">{post.business.username}</h3>
                <p className="u-gallery-text">{post.description}</p>
            </div>
        </div>
    });




    return (
        <Page>


            <Container className={"white-background "}>
                <body data-home-page="Profile.html" data-home-page-title="Profile" className="u-body">

                <CustomModal handleClose={handleCloseModal} showModal={showModal}>
                    <div>
                        <div>
                            {selectedPost}
                        </div>
                    </div>
                </CustomModal>
                <section className="u-clearfix u-section-1" id="carousel_c6fe">
                    <div className="u-clearfix u-layout-wrap u-layout-wrap-1">
                        <div className="u-layout">
                            <div className="u-layout-row">
                                <div className="u-align-left u-container-style u-layout-cell u-size-30 u-layout-cell-1">
                                    <div
                                        className="u-container-layout u-valign-middle-lg u-valign-middle-xl u-container-layout-1">
                                        <h2 className="u-custom-font u-font-merriweather u-text u-text-1">{client.firstName} {client.lastName} ({client.username})</h2>
                                        <div
                                            className="u-border-3 u-border-grey-dark-1 u-expanded-width u-line u-line-horizontal u-line-1"></div>
                                        <p className="u-text u-text-3">{client.bio}</p>

                                        <div
                                            className="u-border-3 u-border-grey-dark-1 u-expanded-width u-line u-line-horizontal u-line-2"></div>
                                        <a href={"/profile/client/" + client.id}
                                           className="u-active-palette-2-dark-3 u-btn u-btn-round u-button-style u-hover-palette-2-dark-3 u-palette-5-light-1 u-radius-50 u-btn-3">Edit
                                            profile</a>
                                    </div>
                                </div>
                                <div className="u-container-style u-image u-layout-cell u-size-30 "
                                     data-image-width="1000" data-image-height="1000">
                                    {client.profileImage &&
                                    <img className={"profileImage"} src={client.profileImage.url}/>}
                                    {!client.profileImage && <img src={photo}/>}

                                    <div className="u-container-layout u-container-layout-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="u-align-center u-clearfix u-grey-10 u-section-3" id="sec-b9ec">
                    <div className="u-clearfix u-sheet u-sheet-1">
                        <h2 className="u-text u-text-1">Gallery</h2>
                        <div
                            className="u-expanded-width u-gallery u-layout-grid u-show-text-on-hover u-gallery-1">
                            <div className="u-gallery-inner u-gallery-inner-1">
                                {postsList}
                            </div>
                        </div>
                        <br/>
                        <br/>
                        {currentUser.client && currentUser.client.id === client.id &&
                        <Button className="button-medium" tag={Link} to={"/posts/"}>Add a new post</Button>}

                    </div>
                </section>


                </body>
            </Container>
        </Page>
    );

};


export default ClientProfile;





