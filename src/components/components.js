import React, {useState} from "react";
import {Modal} from "react-bootstrap";
import {LoginForm, RegisterForm} from "./forms"
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../actions/auth";
import CookieConsent from "react-cookie-consent";


// Custom modal
export function CustomModal(props) {
    const {handleShow, handleClose, showModal} = props;

    return (

        <Modal show={showModal} onHide={handleClose}>
            {props.children}
        </Modal>
    );
}

// The gold/primary button
export function GoldButton(props) {
    return (
        <button className={"gold-button"}>
            {props.children}
        </button>
    )
}

export function TransparentButton(props) {

    return (
        <button className={"gold-button"}>
            {props.children}
        </button>
    )
}


// Navbar
export function Navbar(props) {

    const dispatch = useDispatch();
    const logOut = () => {
        dispatch(logout());
    };

    const {user: user} = useSelector((state) => state.auth);

    const [className, setClassName] = useState("topnav")
    const [showLogin, setShowLogin] = useState(false);
    const [showRegister, setShowRegister] = useState(false);

    const handleCloseLogin = () => setShowLogin(false);
    const handleShowLogin = () => setShowLogin(true);
    const handleCloseRegister = () => setShowRegister(false);
    const handleShowRegister = () => setShowRegister(true)

    return (<div className={className}>
        <CustomModal handleClose={handleCloseLogin} showModal={showLogin}><LoginForm/></CustomModal>
        <CustomModal handleClose={handleCloseRegister} showModal={showRegister}><RegisterForm/></CustomModal>


        <a className={"active nav-left"} href={"/"}>Home</a>
        {user && user.client && <a href="/search" className={"nav-left"}>Book</a>}
        {user && <a href="/discover" className={"nav-left"}>Discover</a>}
        {!user && <a className={"nav-right login-btn"} onClick={handleShowLogin}>Log in</a>}
        {!user && <a className={"nav-right signup-btn"} onClick={handleShowRegister}>Sign up</a>}


        {user && <a href="/" className={"nav-right"} onClick={logOut}>
            Log out
        </a>}
        {user && user.employee && <a className={"nav-right"} href={"/profile"}>Profile</a>}
        {user && user.client && <a className={"nav-right"} href={"/client-profile"}>Profile</a>}

        {user && user.employee && <a className={"nav-left"} href={"/appointments"}> Appointments</a>}
        {user && user.client && <a className={"nav-left"} href={"/client-appointments"}> Appointments</a>}

        {user && <a className={"nav-left"} href={"/posts"}> New post</a>}
    </div>)

}

// Footer
function Footer() {
    return <footer>
        <p>Appointment App, sc17sdc &copy; 2021, <a href={"/terms"} target="_blank">Terms and Conditions</a>, <a
            href={"/policy"} target="_blank">Privacy Policy</a></p>

    </footer>
}

// Page
export function Page(props) {
    const {user} = props

    return <React.Fragment>
        <Navbar/>
        {props.children}
        <CookieConsent>This website uses cookies to enhance the user experience.</CookieConsent>
        <Footer/>
    </React.Fragment>
}