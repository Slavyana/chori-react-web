import React, {useEffect, useState} from 'react';
import {Container, Form, FormGroup, Input, Label} from 'reactstrap';

import {Page} from "./components";
import UploadService from "../services/upload-files.service";
import photo from "../static/images/photo-icon.png"
import {useSelector} from "react-redux";
import Select from "react-select";
import {getBusinessServices} from "../actions/business";
import {ReactSearchAutocomplete} from "react-search-autocomplete";

let options = []

export function PostEdit(props) {

    let handleCancel = (event) => {
        props.history.push('/profile');
    }

    let emptyPost = {
        image: null,
        author: {},
        description: '',
        business: {},
        services: []
    };

    const {user: currentUser} = useSelector((state) => state.auth);

    const [isLoaded, setIsLoaded] = useState(false);
    const id = props.match.params.id;
    const [post, setPost] = useState(emptyPost);
    const [error, setError] = useState(null);
    const [business, setBusiness] = useState({});

    const [selectedFiles, setSelectedFiles] = useState(undefined);
    const [currentFile, setCurrentFile] = useState(undefined);
    const [progress, setProgress] = useState(0);
    const [message, setMessage] = useState("");
    const [services, setServices] = useState([]);
    const [selectedOption, setSelectedOption] = useState([]);
    const [businessSearchList, setBusinessSearchList] = useState([]);

    const [fileInfos, setFileInfos] = useState([]);


    useEffect(() => {
        if(currentUser.employee) {
            setBusiness(currentUser.employee.business)
        }
    }, [])


    useEffect(() => {
        if(id) {

            fetch(`http://localhost:8080/api/posts/${id}`)
                .then(res => res.json())
                .then(
                    (result) => {
                        setIsLoaded(true);
                        setPost(result);
                    },
                    (error) => {
                        setIsLoaded(true);
                        setError(error);
                    }
                )
        }

    }, [business])

    const selectFile = (event) => {
        setSelectedFiles(event.target.files);
    };

    useEffect(() => {
        // options = []
        if (business) {
            getBusinessServices(business.id)
                .then((response) => {
                    setServices(response);
                });
        }

    }, [business])


    useEffect(() => {
        services.map((service => options.push({value: JSON.stringify(service), label: service.name})))
    }, [services])

    useEffect(() => {
        let changedPost = {...post};
        let arr = [];
        selectedOption.map(option => arr.push(JSON.parse(option.value)));
        changedPost["services"] = arr;
        setPost(changedPost);

    }, [selectedOption])


    const upload = () => {
        let currentFile = selectedFiles[0];

        setProgress(0);
        setCurrentFile(currentFile);

        UploadService.upload(currentFile, (event) => {
            setProgress(Math.round((100 * event.loaded) / event.total));
        })
            .then((response) => {
                setMessage(response.data.message);
                return UploadService.getFiles();
            })
            .then((files) => {

                let changedPost = {...post};

                let file = files.data.slice(-1)[0]
                changedPost["image"] = file;

                setPost(changedPost);

                setFileInfos(files.data);
            })
            .catch(() => {
                setProgress(0);
                setMessage("Could not upload the file!");
                setCurrentFile(undefined);
            });

        setSelectedFiles(undefined);
    };

    useEffect(() => {
        UploadService.getFiles().then((response) => {
            setFileInfos(response.data);
        });
    }, []);

    const title = <h2>{post.id ? 'Edit post' : 'Create a new post'}</h2>;


    let handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let changedPost = {...post};
        changedPost[name] = value;
        setPost(changedPost);
    }

    let handleSubmit = (event) => {
        event.preventDefault();
        console.log(post);


        fetch('http://localhost:8080/api/posts' + (id ? '/' + id : ''), {
            method: (id) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(post),
        }).then(() => {
            if( currentUser.client )
            props.history.push('/client-profile')

            else {
                props.history.push('/profile')
            }

        });
    }

    useEffect(() => {
        if (selectedFiles) {
            upload();
        } else {
            let changedPost = {...post};
            changedPost["image"] = null;
            setPost(changedPost);
        }
    }, [selectedFiles])


    useEffect(() => {
        let changedPost = {...post};
        changedPost["business"] = business;
        changedPost["author"] = {...currentUser, roles: null};
        setPost(changedPost);
        console.log(changedPost)

    }, [business, currentUser])


    const handleOnSelect = (item) => {
        console.log(item)
        setBusiness(item)
    }

    const handleOnSearch = (string, results) => {
        console.log(string, results)
        changeSearch(string)
    }

    const changeSearch = (e) => {
        console.log(e)
        fetch('http://localhost:8080/api/businesses?search=' + e)
            .then(response => response.json())
            .then(data => {
                let list = data.map(d => {
                    return {...d, "name": `${d.username}, ${d.name} (${d.region})`}
                });
                setBusinessSearchList(list)
            });
    }

    return (
        <Page>

            <Container className={"white-background padding"}>
                <Form className={"new-form"} onSubmit={handleSubmit}>
                    {title}

                    <div className={"white-background"}>

                        <label className="btn btn-default">
                            <input type="file" onChange={selectFile}/>
                        </label>

                        {currentFile && (
                            <div className="progress">
                                <div
                                    className="progress-bar progress-bar-info progress-bar-striped"
                                    role="progressbar"
                                    aria-valuenow={progress}
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                    style={{width: progress + "%"}}>
                                    {progress}%
                                </div>
                            </div>
                        )}

                        <br/>
                        <div className="card">
                            <div className="card-header">Image</div>

                            <div className="alert-light" role="alert">
                                {message}
                            </div>
                            {post.image && <div>
                                <br/>

                                <img src={post.image && post.image.url} alt={"Post Image"}/>
                                <br/>
                                <button className={"button-medium"} onClick={(e) => {
                                    e.preventDefault();
                                    setSelectedFiles(null);
                                }}>Delete image
                                </button>
                            </div>
                            }
                            {!post.image && <img src={photo} alt={"Profile Image"}/>}

                        </div>
                    </div>

                    <br/>

                    <FormGroup>
                        <Label for="description">Description</Label>
                        <Input type="textarea" name="description" id="description" value={post.description || ''}
                               onChange={handleChange} autoComplete="description"/>
                    </FormGroup>



                    { currentUser.client &&
                    <FormGroup>
                        <Label for="search">Business</Label>

                        <ReactSearchAutocomplete
                        items={businessSearchList}
                        fuseOptions={{keys: ["name"]}}
                        resultStringKeyName="name"
                        onSelect={handleOnSelect}
                        onSearch={handleOnSearch}
                        autoFocus
                    />
                    </FormGroup>
                        }



                    <FormGroup>
                        <Label for="services">Services</Label>
                        <Select
                            isMulti
                            name="colors"
                            options={options}
                            defaultValue={selectedOption}
                            onChange={setSelectedOption}
                            className="basic-multi-select"
                            classNamePrefix="select"
                        />
                    </FormGroup>

                    <div>
                        <button className={"button-primary"} type={"submit"}>Save</button>
                        <button className={"button-secondary"} onClick={handleCancel}>Cancel</button>
                        <br/>
                    </div>

                </Form>
            </Container>
        </Page>);
}