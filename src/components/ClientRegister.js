import React, {useState, useRef, useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import { useHistory } from "react-router-dom";

import { register, login } from "../actions/auth";
import {Login} from "./Login";

const required = (value) => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

const validEmail = (value) => {
    if (!isEmail(value)) {
        return (
            <div className="alert alert-danger" role="alert">
                This is not a valid email.
            </div>
        );
    }
};

const vusername = (value) => {
    if (value.length < 3 || value.length > 20) {
        return (
            <div className="alert alert-danger" role="alert">
                The username must be between 3 and 20 characters.
            </div>
        );
    }
};

const vpassword = (value) => {
    if (value.length < 6 || value.length > 40) {
        return (
            <div className="alert alert-danger" role="alert">
                The password must be between 6 and 40 characters.
            </div>
        );
    }
};


export function ClientRegister(props) {
    const form = useRef();
    const checkBtn = useRef();

    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [successful, setSuccessful] = useState(false);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [dateOfBirth, setDateOfBirth] = useState("");


    // const [lastName, setLastName] = useState("");


    const [agree, setAgree] = useState(false);


    const { message } = useSelector(state => state.message);
    const dispatch = useDispatch();

    const onChangeUsername = (e) => {
        const username = e.target.value;
        setUsername(username);
    };

    const onChangeEmail = (e) => {
        const email = e.target.value;
        setEmail(email);
    };

    const onChangePassword = (e) => {
        const password = e.target.value;
        setPassword(password);
    };

    const onChangeFirstName = (e) => {
        const firstName = e.target.value;
        setFirstName(firstName);
    };

    const onChangeLastName = (e) => {
        const lastName = e.target.value;
        setLastName(lastName);
    };

    const onChangeDateOfBirth = (e) => {
        const dob = e.target.value;
        setDateOfBirth(dob);
    };


    const onChangePhoneNumber = (e) => {
        const phoneNumber = e.target.value;
        setPhoneNumber(phoneNumber);
    };

    const history = useHistory();

    const handleRegister = (e) => {
        e.preventDefault();

        setSuccessful(false);

        form.current.validateAll();

        if (checkBtn.current.context._errors.length === 0) {

            dispatch(register(username, email, password, firstName, lastName, phoneNumber, dateOfBirth ))
                .then((response) => {

                    setSuccessful(true);

                    dispatch(login(username, password))
                        .then(() => {
                            history.push("/client-profile");
                            window.location.reload();
                        })
                        .catch((e) => {
                            console.log("error")
                            console.log(e)

                            // setLoading(false);
                        });


                })
                .catch(() => {
                    console.log("error")

                    setSuccessful(false);
                });




        }
    };


    useEffect(() => {
        const script = document.createElement('script');

        script.src = "../nicepage/nicepage.js";
        script.async = true;

        document.body.appendChild(script);

    }, []);

    return (
        <div>
            <Form className={"login-form"} onSubmit={handleRegister} ref={form}>
                { (
                    <div>
                        <br/>

                        Username
                        <Input
                            type="username"  name="username" placeholder="Enter username"
                            value={username}
                            onChange={onChangeUsername}
                            validations={[required, vusername]}/>

                        Email
                        <Input type="email" name="email" placeholder="Enter email"
                               value={email}
                               onChange={onChangeEmail}
                               validations={[required, validEmail]}/>

                        Password
                        <Input type="password" name="password" placeholder="Enter password"
                               value={password}
                               onChange={onChangePassword}
                               validations={[required, vpassword]}/>

                        First name
                        <Input type="username" name="username" placeholder="Enter first name"
                               value={firstName}
                               onChange={onChangeFirstName}
                               validations={[required]}/>

                        Last name
                        <Input type="username" name="username" placeholder="Enter last name"
                               value={lastName}
                               onChange={onChangeLastName}
                               validations={[required]}/>

                        Phone number
                        <Input type="text"
                               placeholder="Enter phone number"
                               value={phoneNumber}
                               onChange={onChangePhoneNumber}
                               validations={[required]}/>

                        Date of birth
                        <Input type="date"
                               placeholder="Enter date of birth"
                               value={dateOfBirth}
                               onChange={onChangeDateOfBirth}
                               validations={[required]}/>


                        <div>
                            <input className={"checkbox"} type="checkbox" id="agree" name="agree"
                                   defaultChecked={agree}
                                   onChange={() => { console.log(!agree); setAgree(!agree)}}/>
                            <label htmlFor="horns">I agree with the <a href={"/terms"} target="_blank">terms and conditions</a> and the <a href={"/policy"} target="_blank">privacy policy</a></label>
                        </div>

                        { agree && <button>Create a new client account</button> }

                        {message && (
                            <div className="form-group">
                                <div className={ successful ? "alert alert-success" : "alert alert-danger" } role="alert">
                                    {message}
                                </div>
                            </div>
                        )}
                        <CheckButton style={{ display: "none" }} ref={checkBtn} />

                    </div>)}
            </Form>
        </div>

        //
        //
        // <div className="col-md-12">
        //     <div className="card card-container">
        //
        //         <Form onSubmit={handleRegister} ref={form}>
        //             {!successful && (
        //                 <div>
        //                     <div className="form-group">
        //                         <label htmlFor="username">Username</label>
        //                         <Input
        //                             type="text"
        //                             className="form-control"
        //                             name="username"
        //                             value={username}
        //                             onChange={onChangeUsername}
        //                             validations={[required, vusername]}
        //                         />
        //                     </div>
        //
        //                     <div className="form-group">
        //                         <label htmlFor="email">Email</label>
        //                         <Input
        //                             type="text"
        //                             className="form-control"
        //                             name="email"
        //                             value={email}
        //                             onChange={onChangeEmail}
        //                             validations={[required, validEmail]}
        //                         />
        //                     </div>
        //
        //                     <div className="form-group">
        //                         <label htmlFor="password">Password</label>
        //                         <Input
        //                             type="password"
        //                             className="form-control"
        //                             name="password"
        //                             value={password}
        //                             onChange={onChangePassword}
        //                             validations={[required, vpassword]}
        //                         />
        //                     </div>
        //
        //                     <div className="form-group">
        //                         <label htmlFor="firstName">First name</label>
        //                         <Input
        //                             type="text"
        //                             className="form-control"
        //                             name="firstName"
        //                             value={firstName}
        //                             onChange={onChangeFirstName}
        //                             validations={[required]}
        //                         />
        //                     </div>
        //
        //                     <div className="form-group">
        //                         <label htmlFor="lastName">Last name</label>
        //                         <Input
        //                             type="text"
        //                             className="form-control"
        //                             name="lastName"
        //                             value={lastName}
        //                             onChange={onChangeLastName}
        //                             validations={[required]}
        //                         />
        //                     </div>
        //
        //                     <div className="form-group">
        //                         <label htmlFor="businessName">Business name</label>
        //                         <Input
        //                             type="text"
        //                             className="form-control"
        //                             name="businessName"
        //                             value={businessName}
        //                             onChange={onChangeBusinessName}
        //                             validations={[required]}
        //                         />
        //                     </div>
        //
        //
        //                     <div className="form-group">
        //                         <label htmlFor="businessCity">Business city</label>
        //                         <Input
        //                             type="text"
        //                             className="form-control"
        //                             name="businessCity"
        //                             value={businessCity}
        //                             onChange={onChangeBusinessCity}
        //                             validations={[required]}
        //                         />
        //                     </div>
        //
        //                     <div className="form-group">
        //                         <label htmlFor="businessAddress">Business address</label>
        //                         <Input
        //                             type="text"
        //                             className="form-control"
        //                             name="businessAddress"
        //                             value={businessAddress}
        //                             onChange={onChangeBusinessAddress}
        //                             validations={[required]}
        //                         />
        //                     </div>
        //
        //
        //                     <div className="form-group">
        //                         <button className="btn btn-primary btn-block">Sign Up</button>
        //                     </div>
        //                 </div>
        //             )}
        //
        //             {message && (
        //                 <div className="form-group">
        //                     <div className={ successful ? "alert alert-success" : "alert alert-danger" } role="alert">
        //                         {message}
        //                     </div>
        //                 </div>
        //             )}
        //             <CheckButton style={{ display: "none" }} ref={checkBtn} />
        //         </Form>
        //     </div>
        // </div>
    );
};

