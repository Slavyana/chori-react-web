import React, {useState, useRef} from "react";
import {useDispatch, useSelector} from "react-redux";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import {isEmail} from "validator";
import {register} from "../actions/auth";
import {Page} from "./components";
import {useHistory} from "react-router-dom";
import {Container} from "reactstrap";

const required = (value) => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

const validEmail = (value) => {
    if (!isEmail(value)) {
        return (
            <div className="alert alert-danger" role="alert">
                This is not a valid email.
            </div>
        );
    }
};

const vusername = (value) => {
    if (value.length < 3 || value.length > 20) {
        return (
            <div className="alert alert-danger" role="alert">
                The username must be between 3 and 20 characters.
            </div>
        );
    }
};

const vpassword = (value) => {
    if (value.length < 6 || value.length > 40) {
        return (
            <div className="alert alert-danger" role="alert">
                The password must be between 6 and 40 characters.
            </div>
        );
    }
};



export function AddEmployee(props) {

    let handleCancel = (event) => {
        props.history.push('/profile');
    }


    const {user: currentUser} = useSelector((state) => state.auth);
    const form = useRef();
    const checkBtn = useRef();

    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [successful, setSuccessful] = useState(false);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [dateOfBirth, setDateOfBirth] = useState("");

    const businessId = currentUser.employee.business.id;
    const businessUsername = currentUser.employee.business.username;
    const businessName = currentUser.employee.business.name;
    const businessCountry = currentUser.employee.business.country;
    const businessRegion = currentUser.employee.business.region;
    const businessAddress = currentUser.employee.business.address;


    const {message} = useSelector(state => state.message);
    const dispatch = useDispatch();

    const onChangeDateOfBirth = (e) => {
        const dob = e.target.value;
        setDateOfBirth(dob);
    };

    const onChangeUsername = (e) => {
        const username = e.target.value;
        setUsername(username);
    };

    const onChangeEmail = (e) => {
        const email = e.target.value;
        setEmail(email);
    };

    const onChangePassword = (e) => {
        const password = e.target.value;
        setPassword(password);
    };

    const onChangeFirstName = (e) => {
        const firstName = e.target.value;
        setFirstName(firstName);
    };

    const onChangeLastName = (e) => {
        const lastName = e.target.value;
        setLastName(lastName);
    };

    const history = useHistory();


    const handleRegister = (e) => {
        e.preventDefault();

        setSuccessful(false);

        form.current.validateAll();


        if (checkBtn.current.context._errors.length === 0) {

            dispatch(register(username, email, password, firstName, lastName, null, dateOfBirth, businessUsername, businessName, businessCountry, businessRegion, businessAddress, ["employee"], businessId))
                .then(() => {
                    setSuccessful(true);
                    history.push("/profile");
                    window.location.reload();
                })
                .catch(() => {
                    setSuccessful(false);
                });
        }
    };

    return (
        <Page style={{height: "100%"}}>

            <Container className={"white-background padding"}>

                <div>
                    {/*<div className="card card-container">*/}

                    <Form className={"new-form"} onSubmit={handleRegister} ref={form}>
                        <h2>Add a new employee</h2>

                        {!successful && (
                            <div>
                                <div className="form-group">
                                    <label htmlFor="username">Username</label>
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="username"
                                        value={username}
                                        onChange={onChangeUsername}
                                        validations={[required, vusername]}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="email">Email</label>
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="email"
                                        value={email}
                                        onChange={onChangeEmail}
                                        validations={[required, validEmail]}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="password">Password</label>
                                    <Input
                                        type="password"
                                        className="form-control"
                                        name="password"
                                        value={password}
                                        onChange={onChangePassword}
                                        validations={[required, vpassword]}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="firstName">First name</label>
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="firstName"
                                        value={firstName}
                                        onChange={onChangeFirstName}
                                        validations={[required]}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="lastName">Last name</label>
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="lastName"
                                        value={lastName}
                                        onChange={onChangeLastName}
                                        validations={[required]}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="lastName">Date of birth</label>
                                    <Input
                                        type="date"
                                        className="form-control"
                                        name="lastName"
                                        value={dateOfBirth}
                                        onChange={onChangeDateOfBirth}
                                        validations={[required]}
                                    />
                                </div>

                                <div>
                                    <button className={"button-primary"} type={"submit"}>Save</button>
                                    <button className={"button-secondary"} onClick={handleCancel}>Cancel</button>
                                    <br/>
                                </div>
                            </div>
                        )}

                        {message && (
                            <div className="form-group">
                                <div className={successful ? "alert alert-success" : "alert alert-danger"} role="alert">
                                    {message}
                                </div>
                            </div>
                        )}
                        <CheckButton style={{display: "none"}} ref={checkBtn}/>
                    </Form>
                </div>
            </Container>
        </Page>

    );
}

export default AddEmployee;