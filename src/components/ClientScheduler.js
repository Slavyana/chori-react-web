import * as React from 'react';
import {
    ScheduleComponent,
    Day,
    WorkWeek,
    Inject,
    ViewDirective, ViewsDirective
} from '@syncfusion/ej2-react-schedule';
import {useEffect, useRef, useState} from "react";
import {Page} from "./components";

import DatePicker from "react-datepicker"

import "react-datepicker/dist/react-datepicker.css";
import {useSelector} from "react-redux";
import {Button, Dropdown} from "react-bootstrap";
import {Container} from "@material-ui/core";

export function ClientScheduler(props) {
    const {user: currentUser} = useSelector((state) => state.auth);
    const [client, setClient] = useState(currentUser.client);

    let scheduleObj = useRef();

    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/api/clients/" + client.id + "/appointments")
            .then(res => res.json())
            .then(
                (result) => {
                    // console.log(result);
                    let subjects = result.map(r => {
                        let description = `${r.business.username}, ${r.business.address}`
                            return {
                                ...r,
                                description: description,
                                subject: r.service.name
                            }

                        }
                    )
                    setAppointments(subjects);
                },
            )
    }, [client])

    let resourceHeaderTemplate = () => {
        return (
            <div className="template-wrap">
                <div className={"resource-image"}>img</div>
                <div className="resource-details">
                    <div className="resource-name">{"name"}</div>
                    <div className="resource-designation">{"more"}</div>
                </div>
            </div>)
    }

    let onPopupOpen = (args) => {
        if (args.type === 'EventContainer') {
            args.cancel = true;
        }
        if (args.type === 'Editor') {
            console.log("Edit")
            alert("If you want to make changes to the calendar, delete the appointment and add a new one.")
            args.cancel = true;
        }
        if (args.type === 'QuickInfo') {
            if (!args.data.id) {
                args.cancel = true;
            }
        }
        if (args.type === 'DeleteAlert') {
            let selectOk = window.confirm("Are you sure you want to delete this appointment?");
            console.log(args)
            args.cancel = true;

            if (selectOk) {
                console.log(args.data.id)
                fetch('http://localhost:8080/api/appointments/' + args.data.id, {
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                }).then(() => scheduleObj.deleteEvent(args.data.id));
            }
        }
    }


    return (
        <Page>
            <br/>

            <Container>
                <form action={"/search"}>
                    <button>Browse salons</button>
                    {' '}
                </form>

            </Container>
            <br/>

            <ScheduleComponent ref={t => scheduleObj = t} width='100%' height='100%' popupOpen={onPopupOpen}
                               resourceHeaderTemplate={resourceHeaderTemplate} eventSettings={{
                dataSource: appointments,
                fields: {
                    id: 'id',
                    subject: {name: 'subject'},
                    isAllDay: {name: 'isAllDay'},
                    description: {name: 'description'},
                    startTime: {name: 'startTime'},
                    endTime: {name: 'endTime'},
                }
            }}


            >
                <ViewsDirective>
                    <ViewDirective option='Day' startHour='09:00' endHour='18:00'
                                   timeScale={{enable: true, slotCount: 4}}/>
                    <ViewDirective option='WorkWeek' startHour='09:00' endHour='18:00' workDays={[1, 2, 3, 4, 5, 6]}
                                   timeScale={{enable: true, slotCount: 5}}/>
                </ViewsDirective>
                <Inject services={[Day, WorkWeek]}/>
            </ScheduleComponent>
        </Page>
    );
}