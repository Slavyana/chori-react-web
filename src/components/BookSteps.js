import * as React from 'react';
import {
    ScheduleComponent,
    Day,
    Week,
    WorkWeek,
    Month,
    Agenda,
    Inject,
    ViewDirective, ViewsDirective
} from '@syncfusion/ej2-react-schedule';
import {useEffect, useRef, useState} from "react";
import {Page} from "./components";

import DatePicker from "react-datepicker"

import "react-datepicker/dist/react-datepicker.css";
import {useSelector} from "react-redux";
import {Button, Dropdown} from "react-bootstrap";
import {Container} from "@material-ui/core";
import {getBusiness, getBusinessEmployees, getBusinessServices} from "../actions/business";


const isSlotAvailable = (currSlot, slotsNeeded, closingTime, bookedSlots) => {
    for (let i = currSlot; i < currSlot + slotsNeeded; i++) {
        if (i >= closingTime) {
            return false;
        } else {
            if (bookedSlots.includes(i)) {
                return false;
            }
        }
    }

    return true;
}


const calculateTime = (slot) => {
    let time = slot * 15;
    let hours = parseInt(time / 60);
    let minutes = time % 60;

    return `${hours}:${("0" + minutes).slice(-2)}`;
}


const minutesToTime = (mins) => {
    let hours = parseInt(mins / 60);
    let minutes = mins % 60;

    return `${hours}:${("0" + minutes).slice(-2)}`;

}

const minutesToSlots = (mins) => {
    return Math.ceil(mins / 15);
}


function ChooseDateTime(props) {

    const {slot, dateSlotData, updateTime, slotsNeeded, selectedEmployeeId} = props;

    const [timeSlot, setTimeSlot] = useState(slot);
    const [slotData, setSlotData] = useState([]);


    const start = 36;
    const end = 72;

    const [date, setDate] = useState(new Date());

    const update = (slot, startTime) => (e) => {
        updateTime(date, slot, startTime);
        setTimeSlot(slot);
    }

    useEffect(() => {
        fetch(`http://localhost:8080/api/employees/${selectedEmployeeId}/slots?date=` + date.toISOString().slice(0, 10))
            .then(res => res.json())
            .then(
                (result) => {
                    console.log("result slots")

                    console.log(result.slots)
                    if (result.length > 0) {
                        setSlotData(result[0].slots)
                    } else {
                        setSlotData([]);
                    }

                }
            )

        setTimeSlot(-1)
        updateTime(date, -1, -1);

    }, [date])

    const handleDateSelect = (d) => {
        setDate(d)
    }

    var slots = [];
    for (var i = start; i < end; i++) {
        if (!slotData.includes(i) && isSlotAvailable(i, slotsNeeded, end, slotData)) {
            let startTime = calculateTime(i);
            slots.push(<div style={i === timeSlot ? mystyle : null} onClick={update(i, startTime)}
                            key={i}>{startTime}</div>);
        } else if (slotData.includes(i)) {
            slots.push(<div style={booked} key={i}>{calculateTime(i)}</div>);
        } else {
            slots.push(<div style={other} key={i}>{calculateTime(i)}</div>);

        }
    }


    // Check if slot in list, if so - disable

    return (<div>
        <DatePicker selected={date} onChange={date => setDate(date)} onSelect={(date) => handleDateSelect(date)}
                    dateFormat="dd/MM/yyyy"/>

        {slots}

    </div>);
}


// Step 2: Choose staff
function ChooseStaff(props) {
    const {employeeData, selectedEmployeeId, setSelectedEmployee} = props;

    const [employeeId, setEmployeeId] = useState(selectedEmployeeId);


    const update = (id, firstName, lastName, dateSlotsList) => (e) => {
        console.log(id)
        setSelectedEmployee({
            id: id,
            firstName: firstName,
            lastName: lastName,
            dateSlotsList: dateSlotsList
        });
        setEmployeeId(id);
    }

    const employeeList = employeeData.map(employee => {
        console.log(employee)
        return <div key={employee.id} style={employeeId === employee.id ? mystyle : null}
                    onClick={update(employee.id, employee.firstName, employee.lastName, employee.dateSlotsList)}>
            <div>
                <h4><b>{employee.firstName} {employee.lastName}</b></h4>

            </div>
        </div>
    });


    return (
        <div className="card">
            {/*<img src="img_avatar.png" alt="Avatar" style="width:100%"/>*/}
            {employeeList}
        </div>);
}

const mystyle = {
    color: "white",
    backgroundColor: "DodgerBlue",
    padding: "10px",
    fontFamily: "Arial"
};


const other = {
    color: "white",
    backgroundColor: "black",
    padding: "10px",
    fontFamily: "Arial"
};


const booked = {
    color: "white",
    backgroundColor: "gray",
    padding: "10px",
    fontFamily: "Arial"
};


// Step 1: Service table
function ServiceTable(props) {

    let {serviceId, setSelectedService, serviceData} = props;
    const [selectedRow, setSelectedRow] = useState(serviceId);


    const selectService = (selectedId, name, duration) => (e) => {
        setSelectedService({
            id: selectedId,
            name: name,
            durationInMinutes: duration,
            durationInSlots: minutesToSlots(duration)
        });
        setSelectedRow(selectedId);
    }


    const serviceList = serviceData.map(service => {
        return <tr key={service.id} style={selectedRow === service.id ? mystyle : null}
                   onClick={selectService(service.id, service.name, service.durationInMinutes)}>
            <td style={{whiteSpace: 'nowrap'}}>{service.name}</td>
            <td style={{whiteSpace: 'nowrap'}}>{service.description}</td>
            <td style={{whiteSpace: 'nowrap'}}>{minutesToTime(service.durationInMinutes)}</td>
            <td style={{whiteSpace: 'nowrap'}}>{service.price} {service.currency}</td>
            <td><input type="checkbox" checked={selectedRow === service.id ? true : false} readOnly={true}/></td>
        </tr>
    });

    return (
        <div>
            <table>
                <thead>
                <tr>
                    <th>Service</th>
                    <th>Description</th>
                    <th>Duration</th>
                    <th>Price</th>
                    <th>Choose</th>

                </tr>
                </thead>

                <tbody>
                {serviceList}
                </tbody>


            </table>
        </div>
    );
}


let serviceName = "";
let serviceId = -1;
let employeeId = 0;
let employeeName = ""
let startTime = "";
let timeSlot = -1;
let serviceDurationInMinutes = -1;
let serviceDurationInSlots = -1;


/*


{
    "id": "607c711631ffc148b49785b9",
    "startTime": "2021-04-03T15:45:53.991",
    "endTime": "2021-04-03T16:45:53.991",
    "service": {
        "id": null,
        "name": "Short haircut",
        "description": null,
        "durationInMinutes": null,
        "price": 13.0,
        "business": null,
        "employees": null
    },
    "employee": {
        "id": "6061b853c10edd3551a486a4",
        "firstName": "John",
        "lastName": "Doe",
        "business": null
    },
    "business": {
        "id": "6061b822c10edd3551a486a2",
        "name": "Hey business",
        "username": "bizusername",
        "city": "Leeds",
        "address": "Address"
    },
    "dateSlots": {
        "date": "2021-04-03T00:00:00.000+00:00",
        "slots": [
            69,
            70,
            71
        ]
    }
}
 */


export function BookAppointment(props) {

    const serviceId = props.match.params.serviceId;
    const [services, setServices] = useState([]);
    const [employees, setEmployees] = useState([]);
    const [selectedService, setSelectedService] = useState({id: serviceId || -1});
    const [selectedEmployee, setSelectedEmployee] = useState({id: -1});
    const [selectedDate, setSelectedDate] = useState();
    const [selectedEndTime, setSelectedEndTime] = useState();
    const [business, setBusiness] = useState();
    const [selectedStartTime, setSelectedStartTime] = useState();
    const [selectedSlots, setSelectedSlots] = useState([-1]);


    // const [employeeName, setEmployeeName] = useState("");
    const businessId = props.match.params.businessId;


    const updateEmployee = (id, name) => {
        employeeId = id;
        employeeName = name;
    }

    useEffect(() => {

        getBusiness(businessId)
            .then((response) => {
                setBusiness(response);
            });

        getBusinessServices(businessId)
            .then((response) => {
                setServices(response);
            });

        getBusinessEmployees(businessId)
            .then((response) => {
                let validEmployees = [];

                response.map(e => {
                    if (e.services && e.services.includes(selectedService.id)) {
                        validEmployees.push(e)
                    }
                })

                // console.log(validEmployees);

                setEmployees(validEmployees);
            });

    }, [selectedService])

    //
    //
    // useEffect(() => {
    //     getBusinessServices(businessId)
    //         .then((response) => {
    //             setServices(response);
    //         });
    //
    //     // getBusinessEmployees(businessId)
    //     //     .then((response) => {
    //     //         let validEmployees = response.map(e => {
    //     //             if(e.services && e.services.includes(selectedService.id)) {
    //     //                 // console.log(e)
    //     //                 return e;
    //     //             }
    //     //         })
    //     //
    //     //         // console.log(validEmployees);
    //     //
    //     //         setEmployees(validEmployees);
    //     //     });
    //
    // }, [selectedEmployee])


    const updateTime = (date, startSlot, startTime) => {
        let slots = []

        for (var i = startSlot; i <= startSlot + selectedService.durationInSlots; i++) {
            slots.push(i);
        }
        // console.log(slots)
        // console.log(startSlot*15 + selectedService.durationInMinutes)
        let endTime = minutesToTime(startSlot * 15 + selectedService.durationInMinutes)
        // console.log(endTime)

        setSelectedDate(date)
        setSelectedStartTime(startTime);
        setSelectedEndTime(endTime)
        setSelectedSlots(slots)
        // console.log()
        // console.log("time", startTime);

    }

    const serviceData = [{id: "1", durationInMinutes: 120, name: "long haircut"}, {
        id: "2",
        durationInMinutes: 125,
        name: "hait trim"
    }, {
        id: "3",
        durationInMinutes: 30,
        name: "short haircut"
    },
        {id: "4", durationInMinutes: 36, name: "nails"}, {id: "5", durationInMinutes: 320, name: "dye hair"}]


    const employeeData = [{id: "1", name: "Joe Doe"}, {id: "2", name: "Jane Trim"}, {
        id: "3",
        name: "Laura haircut"
    },
        {id: "4", name: "John Smith"}, {id: "5", name: "Boy Doe"}]

    const bookedSlots = [50, 52, 53];


    const [step, setStep] = useState(1);

    const nextClick = () => {
        setStep(step + 1);
        console.log(step);
        console.log({
            serviceId: serviceId,
            serviceName: serviceName,
            employeeId: employeeId,
            employeeName: employeeName,
            startTime: startTime,
            endTime: startTime,
            slots: timeSlot

        });
    }

    const prevClick = () => {
        console.log(step);
        setStep(step - 1);
        console.log(step);
    }

    let onFormSubmit = () => {

        // let startDateTime = new Date(selectedDate + " " + selectedStartTime);
        // console.log(startDateTime)
        let h = selectedStartTime.split(":")[0];
        let m = selectedStartTime.split(":")[1];
        let startTime = new Date(selectedDate.toISOString());
        startTime.setUTCHours(h, m, 0)


        h = selectedEndTime.split(":")[0];
        m = selectedEndTime.split(":")[1];
        let endTime = new Date(selectedDate.toISOString());
        endTime.setUTCHours(h, m, 0)


        // console.log(startTime)
        // let startDateTime = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate, ...s);
        // console.log(startDateTime)
        // selectedDate.setHours(12, 12, 12)
        // console.log(endTime.toISOString())

        console.log("SLOT DATE")
        console.log(selectedDate)


        let appointment = {
            startTime: startTime,
            endTime: endTime,
            service: selectedService,
            employee: selectedEmployee,
            business: business,
            dateSlots: {
                date: selectedDate,
                slots: selectedSlots
            }
        }

        console.log(appointment)


        fetch('http://localhost:8080/api/appointments', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(appointment),
        }).then(() => props.history.push('/appointments'));
    }


    return (
        <Page>
            <Container className={"white-background padding container"}>
                <div>
                    {step === 1 && <ServiceTable serviceId={selectedService.id} setSelectedService={setSelectedService}
                                                 serviceData={services}/>}
                    {step === 2 && <ChooseStaff employeeData={employees} selectedEmployeeId={selectedEmployee.id}
                                                setSelectedEmployee={setSelectedEmployee}/>}
                    {step === 3 && <ChooseDateTime slotsNeeded={selectedService.durationInSlots}
                                                   dateSlotData={selectedEmployee.dateSlotsList || []}
                                                   updateTime={updateTime} slot={selectedSlots[0]} selectedEmployeeId={selectedEmployee.id}/>}
                    {step === 4 && <div>

                        <ul>
                            <li>{selectedService.name}</li>
                            <li>{selectedEmployee.firstName}</li>
                            <li>{selectedDate.toString()}</li>

                            <li>{selectedStartTime} - {selectedEndTime}</li>
                        </ul>

                    </div>}

                    {step > 1 && <button className={"button-secondary"} onClick={prevClick}>Previous</button>}
                    {step < 4 && <button className={"button-primary"} onClick={nextClick}>Next</button>}
                    {step === 4 && <button onClick={onFormSubmit}>Submit</button>}

                </div>
            </Container>

        </Page>

    );
}
