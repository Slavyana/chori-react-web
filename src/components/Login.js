import React, {useState, useRef} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom';

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import {login} from "../actions/auth";

const required = (value) => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

export function Login(props) {

    const {account} = props;
    const form = useRef();
    const checkBtn = useRef();

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);

    const {isLoggedIn} = useSelector(state => state.auth);
    const {message} = useSelector(state => state.message);

    const dispatch = useDispatch();

    const onChangeUsername = (e) => {
        const username = e.target.value;
        setUsername(username);
    };

    const onChangePassword = (e) => {
        const password = e.target.value;
        setPassword(password);
    };

    const history = useHistory();

    const handleLogin = (e) => {
        e.preventDefault();

        setLoading(true);

        form.current.validateAll();

        if (checkBtn.current.context._errors.length === 0) {
            dispatch(login(username, password))
                .then(() => {
                    if(account === "client") history.push("/client-profile");
                    else history.push("/profile");

                    window.location.reload();
                })
                .catch(() => {
                    setLoading(false);
                });
        } else {
            setLoading(false);
        }
    };


    return (
        <Form className={"login-form"} onSubmit={handleLogin} ref={form}>
            <br/>
            {account === "business" && <span>Employee username</span>}
            {account === "client" && <span>Username</span>}

            <Input
                type="username"
                name="username"
                value={username}
                onChange={onChangeUsername}
                validations={[required]}
            />


            Password
            <Input
                type="password"
                name="password"
                value={password}
                onChange={onChangePassword}
                validations={[required]}
            />


            <button disabled={loading}>
                {loading && (
                    <span className="spinner-border spinner-border-sm"/>
                )}

                {account === "business" && <span>Log in to business account</span>}
                {account === "client" && <span>Log in to user account</span>}

            </button>

            {message && (
                <div className="form-group">
                    <div className="alert alert-danger" role="alert">
                        {message}
                    </div>
                </div>
            )}
            <CheckButton style={{display: "none"}} ref={checkBtn}/>
        </Form>

    );
}

