import * as React from 'react';
import {
    ScheduleComponent,
    Day,
    WorkWeek,
    Inject,
    ViewDirective, ViewsDirective
} from '@syncfusion/ej2-react-schedule';
import {useEffect, useRef, useState} from "react";
import {Page} from "./components";

import DatePicker from "react-datepicker"

import "react-datepicker/dist/react-datepicker.css";
import {useSelector} from "react-redux";
import {Button, Dropdown} from "react-bootstrap";
import {Container} from "@material-ui/core";
import {getBusiness, getBusinessEmployees, getBusinessServices} from "../actions/business";

const isSlotAvailable = (currSlot, slotsNeeded, closingTime, bookedSlots) => {
    for (let i = currSlot; i < currSlot + slotsNeeded; i++) {
        if (i >= closingTime) {
            return false;
        } else {
            if (bookedSlots.includes(i)) {
                return false;
            }
        }
    }
    return true;
}


const calculateTime = (slot) => {
    let time = slot * 15;
    let hours = parseInt(time / 60);
    let minutes = time % 60;

    return `${hours}:${("0" + minutes).slice(-2)}`;
}


const minutesToTime = (mins) => {
    let hours = parseInt(mins / 60);
    let minutes = mins % 60;

    return `${hours}:${("0" + minutes).slice(-2)}`;

}

const minutesToSlots = (mins) => {
    return Math.ceil(mins / 15);
    console.log(minutesToSlots());
}


function ChooseDateTime(props) {

    const {slot, dateSlotData, updateTime, slotsNeeded, selectedEmployeeId} = props;

    const [timeSlot, setTimeSlot] = useState(slot);
    const [slotData, setSlotData] = useState([]);


    const start = 36;
    const end = 72;

    const [date, setDate] = useState(new Date());

    const update = (slot, startTime) => (e) => {
        updateTime(date, slot, startTime);
        setTimeSlot(slot);
    }

    useEffect(() => {
        fetch(`http://localhost:8080/api/employees/${selectedEmployeeId}/slots?date=` + date.toISOString().slice(0, 10))
            .then(res => res.json())
            .then(
                (result) => {
                    console.log("result slots")

                    console.log(result.slots)
                    if (result.length > 0) {
                        setSlotData(result[0].slots)
                    } else {
                        setSlotData([]);
                    }

                }
            )

        setTimeSlot(-1)
        updateTime(date, -1, -1);

    }, [date])

    const handleDateSelect = (d) => {
        setDate(d)
    }

    var slots = [];
    for (var i = start; i < end; i++) {
        if (!slotData.includes(i) && isSlotAvailable(i, slotsNeeded, end, slotData)) {
            let startTime = calculateTime(i);
            slots.push(<div style={i === timeSlot ? selectedStyle : {height:35}} onClick={update(i, startTime)}
                            key={i}>{startTime}</div>);
        }
        // else if (slotData.includes(i)) {
        //     slots.push(<div style={booked} key={i}>{calculateTime(i)}</div>);
        // } else {
        //     slots.push(<div style={other} key={i}>{calculateTime(i)}</div>);
        //
        // }
    }

    // Check if slot in list, if so - disable

    return (<div>
        <DatePicker selected={date} onChange={date => setDate(date)} onSelect={(date) => handleDateSelect(date)}
                    dateFormat="dd/MM/yyyy"/>

        {slots}

    </div>);
}


// Step 2: Choose staff
function ChooseStaff(props) {
    let {employeeData, selectedEmployeeId, setSelectedEmployee} = props;

    const [employeeId, setEmployeeId] = useState(selectedEmployeeId);

    const {user: currentUser} = useSelector((state) => state.auth);


    const update = (id, firstName, lastName, dateSlotsList) => (e) => {
        console.log(id)
        setSelectedEmployee({
            id: id,
            firstName: firstName,
            lastName: lastName,
            dateSlotsList: dateSlotsList
        });
        setEmployeeId(id);
    }

    const employeeList = employeeData.map(employee => {
        console.log(employee)
        return <div key={employee.id} style={employeeId === employee.id ? selectedStyle : null}
                    onClick={update(employee.id, employee.firstName, employee.lastName, employee.dateSlotsList)}>
            <div>
                <h4><b>{employee.firstName} {employee.lastName}</b></h4>

            </div>
        </div>
    });


    return (
        <div className="card">
            {employeeList}
        </div>);
}

const selectedStyle = {
    backgroundColor: "lightgrey",
    padding: "5px",
    height: 75
};


// Step 1: Service table
function ServiceTable(props) {

    let {serviceId, setSelectedService, serviceData} = props;
    const [selectedRow, setSelectedRow] = useState(serviceId);

    const selectService = (selectedId, name, duration) => (e) => {
        setSelectedService({
            id: selectedId,
            name: name,
            durationInMinutes: duration,
            durationInSlots: minutesToSlots(duration)
        });
        setSelectedRow(selectedId);
    }

    const serviceList = serviceData.map(service => {
        return <tr key={service.id} style={selectedRow === service.id ? selectedStyle : {height: "75px"}}
                   onClick={selectService(service.id, service.name, service.durationInMinutes)}>
            <td className="u-border-1 u-border-grey-40 u-border-no-left u-border-no-right u-table-cell"> {service.name}
            </td>
            <td className="u-border-1 u-border-grey-40 u-border-no-left u-border-no-right u-table-cell">{service.description}</td>
            <td className="u-border-1 u-border-grey-40 u-border-no-left u-border-no-right u-table-cell">{service.price} {service.currency}</td>
            <td className="u-border-1 u-border-grey-40 u-border-no-left u-border-no-right u-table-cell"><input
                type="checkbox" checked={selectedRow === service.id ? true : false} readOnly={true}/></td>
        </tr>
    });

    return (
        <div>
            <div className="u-expanded-width u-table u-table-responsive u-table-1">
                <table className="u-table-entity">
                    <colgroup>
                        <col width="25%"/>
                        <col width="25%"/>
                        <col width="25%"/>
                        <col width="25%"/>
                    </colgroup>
                    <thead className="u-grey-50 u-table-header u-table-header-1">
                    <tr style={{height: "21px"}}>
                        <th className="u-border-1 u-border-grey-50 u-table-cell">Service</th>
                        <th className="u-border-1 u-border-grey-50 u-table-cell">Description</th>
                        <th className="u-border-1 u-border-grey-50 u-table-cell">Price</th>
                        <th className="u-border-1 u-border-grey-50 u-table-cell">Choose</th>
                    </tr>
                    </thead>

                    <tbody className="u-table-body">
                    {serviceList}
                    </tbody>
                </table>
            </div>
        </div>
    );
}


let serviceName = "";
let serviceId = -1;
let employeeId = 0;
let employeeName = ""
let startTime = "";
let timeSlot = -1;
let serviceDurationInMinutes = -1;
let serviceDurationInSlots = -1;


export function BookAppointment(props) {

    const serviceId = props.match.params.serviceId;
    const [services, setServices] = useState([]);
    const [employees, setEmployees] = useState([]);
    const [selectedService, setSelectedService] = useState({id: serviceId});
    const [selectedEmployee, setSelectedEmployee] = useState({id: -1});
    const [selectedDate, setSelectedDate] = useState();
    const [selectedEndTime, setSelectedEndTime] = useState();
    const [business, setBusiness] = useState();
    const [selectedStartTime, setSelectedStartTime] = useState();
    const [selectedSlots, setSelectedSlots] = useState([-1]);
    const {user: currentUser} = useSelector((state) => state.auth);

    const businessId = props.match.params.businessId;


    const updateEmployee = (id, name) => {
        employeeId = id;
        employeeName = name;
    }

    useEffect(() => {
        if (serviceId && services.length > 0) {
            let result = services.find(s => s.id === serviceId);
            let finalResult = {...result, durationInSlots: minutesToSlots(result.durationInMinutes)}
            setSelectedService(finalResult);
        }

    }, [services])

    useEffect(() => {

        getBusiness(businessId)
            .then((response) => {
                setBusiness(response);
            });

        if ((currentUser.employee && currentUser.roles && currentUser.roles.includes("ROLE_MANAGER")) || currentUser.client) {
            getBusinessServices(businessId)
                .then((response) => {
                    setServices(response);
                });
        } else if (currentUser.employee) {
            fetch("http://localhost:8080/api/employees/" + currentUser.employee.id + "/services")
                .then(res => res.json())
                .then(
                    (result) => {
                        setServices(result)
                    },
                )
        }


        getBusinessEmployees(businessId)
            .then((response) => {
                let validEmployees = [];

                response.map(e => {
                    if (e.services && e.services.includes(selectedService.id)) {
                        validEmployees.push(e)
                    }
                })

                // console.log(validEmployees);

                if (currentUser.employee && currentUser.roles && !currentUser.roles.includes("ROLE_MANAGER")) {
                    let result = []
                    result.push(currentUser.employee)
                    setEmployees(result);
                } else {
                    setEmployees(validEmployees);
                }
            });

    }, [selectedService])


    const updateTime = (date, startSlot, startTime) => {
        let slots = []

        for (let i = startSlot; i < startSlot + selectedService.durationInSlots; i++) {
            slots.push(i);
        }
        let endTime = minutesToTime(startSlot * 15 + selectedService.durationInMinutes)

        setSelectedDate(date)
        setSelectedStartTime(startTime);
        setSelectedEndTime(endTime)
        setSelectedSlots(slots)
    }


    const [step, setStep] = useState(1);

    const nextClick = () => {
        setStep(step + 1);
        console.log(step);
        console.log({
            serviceId: serviceId,
            serviceName: serviceName,
            employeeId: employeeId,
            employeeName: employeeName,
            startTime: startTime,
            endTime: startTime,
            slots: timeSlot

        });
    }

    const prevClick = () => {
        console.log(step);
        setStep(step - 1);
        console.log(step);
    }

    let onFormSubmit = () => {
        let h = selectedStartTime.split(":")[0];
        let m = selectedStartTime.split(":")[1];
        let startTime = new Date(selectedDate.toISOString());
        startTime.setUTCHours(h, m, 0)

        h = selectedEndTime.split(":")[0];
        m = selectedEndTime.split(":")[1];
        let endTime = new Date(selectedDate.toISOString());
        endTime.setUTCHours(h, m, 0)

        let client = currentUser.client ? currentUser.client : null;

        let appointment = {
            startTime: startTime,
            endTime: endTime,
            service: selectedService,
            employee: selectedEmployee,
            business: business,
            client: client,
            dateSlots: {
                date: selectedDate,
                slots: selectedSlots
            }
        }

        fetch('http://localhost:8080/api/appointments', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(appointment),
        }).then(() => currentUser.employee ? props.history.push('/appointments') : props.history.push('/client-appointments'));
    }


    return (
        <Page>
            <Container className={"white-background padding container"}>
                <div>
                    {step === 1 && <ServiceTable serviceId={selectedService.id} setSelectedService={setSelectedService}
                                                 serviceData={services}/>}
                    {step === 2 && <ChooseStaff employeeData={employees} selectedEmployeeId={selectedEmployee.id}
                                                setSelectedEmployee={setSelectedEmployee}/>}
                    {step === 3 && <ChooseDateTime slotsNeeded={selectedService.durationInSlots}
                                                   dateSlotData={selectedEmployee.dateSlotsList || []}
                                                   updateTime={updateTime} slot={selectedSlots[0]}
                                                   selectedEmployeeId={selectedEmployee.id}/>}
                    {step === 4 && <div>

                        <ul>
                            <li>Service: {selectedService.name}</li>
                            <li>Employee: {selectedEmployee.firstName}</li>
                            <li>Date: {selectedDate.toISOString().slice(0,10)}</li>

                            <li>Time: {selectedStartTime} - {selectedEndTime}</li>
                        </ul>

                    </div>}
                    <br/>
                    <br/>

                    <div>
                        {step > 1 && <button className={"button-secondary"} onClick={prevClick}>Previous</button>}
                        {step < 4 && <button className={"button-primary"} onClick={nextClick}>Next</button>}
                        {step === 4 && <button className={"button-primary"} onClick={onFormSubmit}>Book!</button>}
                    </div>


                </div>
            </Container>

        </Page>

    );
}


export function Scheduler(props) {
    const {user: currentUser} = useSelector((state) => state.auth);
    const [employee, setEmployee] = useState(currentUser.employee);
    const [employees, setEmployees] = useState([]);

    if (!currentUser.employee) {
        props.history.push('/client-appointments')
    }

    useEffect(() => {
        try {
            getBusinessEmployees(currentUser.employee.business.id)
                .then((response) => {
                    setEmployees(response);
                });
        } catch {
            props.history.push('/client-appointments')
        }

    }, [currentUser])

    let employeesList = employees.map(e => <Dropdown.Item
        onClick={() => setEmployee(e)}>{e.firstName} {e.lastName}, {e.username}</Dropdown.Item>)


    let scheduleObj = useRef();


    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/api/employees/" + employee.id + "/appointments")
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    let subjects = result.map(r => {

                            let description = r.client ? `${r.client.username}, ${r.client.phoneNumber}` : `Manually added.`
                            return {
                                ...r,
                                subject: r.service.name,
                                description: description
                            }

                        }
                    )
                    setAppointments(subjects);
                },
            )


    }, [employee])

    let resourceHeaderTemplate = () => {
        return (
            <div className="template-wrap">
                <div className={"resource-image"}>img</div>
                <div className="resource-details">
                    <div className="resource-name">{"name"}</div>
                    <div className="resource-designation">{"more"}</div>
                </div>
            </div>)
    }

    let onPopupOpen = (args) => {
        if (args.type === 'EventContainer') {
            args.cancel = true;
        }
        if (args.type === 'Editor') {
            alert("If you want to make changes to the calendar, delete the appointment and add a new one.")
            args.cancel = true;
        }
        if (args.type === 'QuickInfo') {
            if (!args.data.id) {
                args.cancel = true;
            }
        }
        if (args.type === 'DeleteAlert') {
            let selectOk = window.confirm("Are you sure you want to delete this appointment?");
            console.log(args)
            args.cancel = true;

            if (selectOk) {
                console.log(args.data.id)
                fetch('http://localhost:8080/api/appointments/' + args.data.id, {
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                }).then(() => scheduleObj.deleteEvent(args.data.id));
            }
        }
    }


    return (
        <Page>
            <br/>

            <Container>
                {currentUser.employee && currentUser.roles && currentUser.roles.includes("ROLE_MANAGER") &&
                <Dropdown>
                    <Dropdown.Toggle className={"button-secondary menu"} id="dropdown-basic">
                        {employee.firstName + " " + employee.lastName}
                    </Dropdown.Toggle>

                    <Dropdown.Menu className={"menu-dropdown"}>
                        {employeesList}
                    </Dropdown.Menu>
                </Dropdown>
                }


                <form action={employee.business.id + "/appointments/add"}>
                    <button className={"button-primary"}>Add a new appointment</button>
                </form>


            </Container>
            <br/>


            <ScheduleComponent ref={t => scheduleObj = t} width='100%' height='100%' popupOpen={onPopupOpen}
                               resourceHeaderTemplate={resourceHeaderTemplate} eventSettings={{
                dataSource: appointments,
                fields: {
                    id: 'id',
                    subject: {name: 'subject'},
                    isAllDay: {name: 'isAllDay'},
                    description: {name: 'description'},
                    startTime: {name: 'startTime'},
                    endTime: {name: 'endTime'},
                }
            }}


            >
                <ViewsDirective>
                    <ViewDirective option='Day' startHour='09:00' endHour='18:00'
                                   timeScale={{enable: true, slotCount: 4}}/>
                    <ViewDirective option='WorkWeek' startHour='09:00' endHour='18:00' workDays={[1, 2, 3, 4, 5, 6]}
                                   timeScale={{enable: true, slotCount: 5}}/>
                </ViewsDirective>
                <Inject services={[Day, WorkWeek]}/>
            </ScheduleComponent>
        </Page>
    );
}