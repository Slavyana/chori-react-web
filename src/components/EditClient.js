import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {Button, Container, Form, FormGroup, Input, Label} from 'reactstrap';
import {AppNavbar} from './AppNavbar';


export function EditClient(props) {

    let emptyClient = {
        firstName: '',
        lastName: '',
    };

    console.log(props)

    const [isLoaded, setIsLoaded] = useState(false);
    const id = props.match.params.id;

    const [client, setClient] = useState(emptyClient);
    const [error, setError] = useState(null);


    useEffect(() => {
        fetch(`http://localhost:8080/api/clients/${id}`)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setClient(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    const title = <h2>{client.id ? 'Edit Client' : 'Add Client'}</h2>;


    let handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let newClient = {...client};
        newClient[name] = value;
        setClient(newClient);
    }

    let handleSubmit = (event) => {
        event.preventDefault();
        fetch('http://localhost:8080/api/clients' + (client.id ? '/' + id : ''), {
            method: (client.id) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(client),
        }).then(() => props.history.push('/client-profile'));
    }

    return (
        <div>
            <AppNavbar/>
            <Container>
                {title}
                <Form onSubmit={handleSubmit}>
                    <FormGroup>
                        <Label for="firstName">First name</Label>
                        <Input type="text" name="firstName" id="firstName" value={client.firstName || ''}
                               onChange={handleChange} autoComplete="firstName"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="lastName">Last name</Label>
                        <Input type="lastName" name="lastName" id="lastName" value={client.lastName || ''}
                               onChange={handleChange} autoComplete="lastName"/>
                    </FormGroup>
                    <FormGroup>
                        <Button color="primary" type="submit">Save</Button>{' '}
                        <Button color="secondary" tag={Link} to="/employees">Cancel</Button>
                    </FormGroup>
                </Form>
            </Container>
        </div>);
}