import React, {useState, useRef} from "react";
import {useDispatch, useSelector} from "react-redux";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import {postService} from "../actions/service";
import {Page} from "./components";
import {Container} from "react-bootstrap";

const required = (value) => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

export function AddService(props) {
    const {user: currentUser} = useSelector((state) => state.auth);
    const form = useRef();
    const checkBtn = useRef();
    const [successful, setSuccessful] = useState(false);

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState();
    const [currency, setCurrency] = useState("GBP");


    const [hours, setHours] = useState();
    const [minutes, setMinutes] = useState();


    let handleCancel = (event) => {
        props.history.push('/profile');
    }

    const businessId = currentUser.employee.business.id;


    const {message} = useSelector(state => state.message);
    const dispatch = useDispatch();

    const onChangeName = (e) => {
        const name = e.target.value;
        setName(name);
    };

    const onChangeDescription = (e) => {
        const description = e.target.value;
        setDescription(description);
    };

    const onChangePrice = (e) => {
        const price = e.target.value;
        setPrice(price);
    };

    const onChangeCurrency = (e) => {
        const currency = e.target.value;
        setCurrency(currency);
    };

    const onChangeHours = (e) => {
        const hours = e.target.value;
        setHours(hours);
    };

    const onChangeMinutes = (e) => {
        const minutes = e.target.value;
        setMinutes(minutes);
    };


    const handleSubmit = (e) => {
        e.preventDefault();

        setSuccessful(false);

        form.current.validateAll();

        if (checkBtn.current.context._errors.length === 0) {
            const duration = parseInt(hours) * 60 + parseInt(minutes);

            postService(businessId, name, description, price, currency, duration)
                .then(() => {
                    setSuccessful(true);
                    props.history.push("/profile");
                })
                .catch(() => {
                    setSuccessful(false);
                });
        }
    };

    return (
        <Page>
            <Container className={"white-background padding"}>
                <Form className={"new-form"} onSubmit={handleSubmit} ref={form}>
                    <h2>Add a new service</h2>
                    {!successful && (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <Input
                                    type="text"
                                    className="form-control"
                                    name="name"
                                    value={name}
                                    onChange={onChangeName}
                                    validations={[required]}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="description">Description</label>
                                <Input
                                    type="text"
                                    className="form-control"
                                    name="description"
                                    value={description}
                                    onChange={onChangeDescription}
                                    validations={[required]}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="price">Price</label>
                                <Input
                                    type="number"
                                    className="form-control"
                                    name="price"
                                    value={price}
                                    onChange={onChangePrice}
                                    validations={[required]}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="price">Currency</label>
                                <Input
                                    type="text"
                                    className="form-control"
                                    name="currency"
                                    value={currency}
                                    onChange={onChangeCurrency}
                                    validations={[required]}
                                />
                            </div>


                            <div className="time-input width-100">
                                <div className="form-group ">
                                    <label htmlFor="duration">Hours</label>

                                    <Input
                                        type="number"
                                        className="form-control"
                                        name="hours"
                                        value={hours}
                                        onChange={onChangeHours}
                                        validations={[required]}
                                        min='0' max='24'
                                    />
                                </div>

                                <div>

                                </div>

                                <div className="form-group ">

                                    <label htmlFor="duration">Minutes</label>

                                    <Input
                                        type="number"
                                        className="form-control"
                                        name="minutes"
                                        value={minutes}
                                        onChange={onChangeMinutes}
                                        validations={[required]}
                                        min='0' max='59'
                                    />
                                </div>

                            </div>
                            <p>Make sure to add this service to the employees from the Edit employee page!</p>


                            <div>
                                <button className={"button-primary"} type={"submit"}>Save</button>
                                <button className={"button-secondary"} onClick={handleCancel}>Cancel</button>
                                <br/>
                            </div>
                        </div>
                    )}

                    {message && (
                        <div className="form-group">
                            <div className={successful ? "alert alert-success" : "alert alert-danger"} role="alert">
                                {message}
                            </div>
                        </div>
                    )}
                    <CheckButton style={{display: "none"}} ref={checkBtn}/>
                </Form>
            </Container>
        </Page>
    );
};

export default AddService;