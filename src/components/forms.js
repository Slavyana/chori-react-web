import React, {useState} from "react";
import {Button, ButtonGroup} from "react-bootstrap";
import {Login} from "./Login"
import {ClientRegister} from "./ClientRegister";
import {CountryDropdown, RegionDropdown} from "react-country-region-selector";
import {BusinessRegister} from "./Register";

// Register form
export function RegisterForm(props) {
    const [showUserForm, setShowUserForm] = useState(true)
    const [userClassName, setUserClassName] = useState("account-select-btn active")
    const [businessClassName, setBusinessClassName] = useState("account-select-btn")

    const handleUserTab = () => {
        setUserClassName("account-select-btn active")
        setBusinessClassName("account-select-btn")
        setShowUserForm(true)
    }

    const handleBusinessTab = () => {
        setUserClassName("account-select-btn")
        setBusinessClassName("account-select-btn active")
        setShowUserForm(false)
    }


    return <div className="login">

        <h2> Sign up </h2>

        <ButtonGroup className={"account-select-btn-group"} aria-label="Basic example">
            <Button className={userClassName} onClick={handleUserTab}>User account</Button>
            <Button className={businessClassName} onClick={handleBusinessTab}>Business account</Button>
        </ButtonGroup>

        {showUserForm && <ClientRegister history={props}/>}

        {!showUserForm && <BusinessRegister history={props}/>}

    </div>
}

// Log in form
export function LoginForm() {
    const [showUserForm, setShowUserForm] = useState(true)
    const [userClassName, setUserClassName] = useState("account-select-btn active")
    const [businessClassName, setBusinessClassName] = useState("account-select-btn")

    const handleUserTab = () => {
        // console.log("moooo")
        setUserClassName("account-select-btn active")
        setBusinessClassName("account-select-btn")
        setShowUserForm(true)
    }

    const handleBusinessTab = () => {
        setUserClassName("account-select-btn")
        setBusinessClassName("account-select-btn active")
        setShowUserForm(false)
    }


    return <div className="login">

        <h2>Log in to</h2>

        {/* or add tabs on top */}
        <ButtonGroup className={"account-select-btn-group"} aria-label="Basic example">
            <Button className={userClassName} onClick={handleUserTab}>User account</Button>
            <Button className={businessClassName} onClick={handleBusinessTab}>Business account</Button>
        </ButtonGroup>

        <div>
            {showUserForm && <Login account={"client"}/>}

            {!showUserForm && <Login account={"business"}/>}

        </div>

    </div>
}

// Booking form on home page
export function HomeBookingForm() {

    const [country, setCountry] = useState('');
    const [region, setRegion] = useState('');



    return <div className="form-style">
        <h1>Book your appointment now</h1>
        <form action={"search"}>
            <label>Country</label>
            <CountryDropdown
                name={"country"}
                className={"form-white"}
                value={country || ''}
                onChange={(val) => {
                    console.log(val)
                    setCountry(val)
                }}/>

            <label>Region</label>
            <RegionDropdown
                name={"region"}
                className={"form-white"}
                country={country}
                value={region || ''}
                onChange={(val) => {
                    console.log(val)

                    setRegion(val)
                }}/>

            <label>Service</label>
            <textarea name="service" placeholder="All services"/>
            <input type="submit" value="Find"/>
        </form>
    </div>
}
