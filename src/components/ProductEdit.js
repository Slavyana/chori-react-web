import React, {useEffect, useState} from 'react';
import {Container, Form, FormGroup, Input, Label} from 'reactstrap';
import {Page} from "./components";
import UploadService from "../services/upload-files.service";
import photo from "../static/images/photo-icon.png"
import {useSelector} from "react-redux";

export function ProductEdit(props) {

    let handleCancel = (event) => {
        props.history.push('/profile');
    }


    let emptyProduct = {
        image: null,
        name: '',
        description: '',
        price: 0.0,
        currency: 'GBP',
        business: {}
    };

    const {user: currentUser} = useSelector((state) => state.auth);

    const id = props.match.params.id;
    const [product, setProduct] = useState(emptyProduct);
    const [business, setBusiness] = useState({});

    const [selectedFiles, setSelectedFiles] = useState(undefined);
    const [currentFile, setCurrentFile] = useState(undefined);
    const [progress, setProgress] = useState(0);
    const [message, setMessage] = useState("");

    const [fileInfos, setFileInfos] = useState([]);

    const selectFile = (event) => {
        setSelectedFiles(event.target.files);
    };

    const upload = () => {
        let currentFile = selectedFiles[0];

        setProgress(0);
        setCurrentFile(currentFile);

        UploadService.upload(currentFile, (event) => {
            setProgress(Math.round((100 * event.loaded) / event.total));
        })
            .then((response) => {
                console.log(response)
                setMessage(response.data.message);
                return UploadService.getFiles();
            })
            .then((files) => {

                let changedProduct = {...product};

                let file = files.data.slice(-1)[0]
                changedProduct["image"] = file;

                setProduct(changedProduct);

                setFileInfos(files.data);
            })
            .catch(() => {
                setProgress(0);
                setMessage("Could not upload the file!");
                setCurrentFile(undefined);
            });

        setSelectedFiles(undefined);
    };

    useEffect(() => {
        UploadService.getFiles().then((response) => {
            setFileInfos(response.data);
        });
    }, []);

    const title = <h2>{id ? 'Edit product details' : 'Add a new product'}</h2>;


    let handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let changedProduct = {...product};
        changedProduct[name] = value;
        setProduct(changedProduct);
    }

    // useEffect(() => {
    //     fetch(`http://localhost:8080/api/products/${id}`)
    //         .then(res => res.json())
    //         .then(
    //             (result) => {
    //                 setProduct(result);
    //             },
    //             (error) => {
    //             }
    //         )
    // }, [])


    let handleSubmit = (event) => {
        event.preventDefault();
        console.log(product)


        fetch('http://localhost:8080/api/products' + (id ? '/' + id : ''), {
            method: (id) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(product),
        }).then(() => props.history.push('/profile'));
    }

    useEffect(() => {
        if (selectedFiles) {
            upload();
        } else {
            let changedProduct = {...product};
            changedProduct["image"] = null;
            setProduct(changedProduct);
        }
    }, [selectedFiles])

    useEffect(() => {
        setBusiness(currentUser.employee.business)
        console.log(business)
    }, [])

    useEffect(() => {
        let changedProduct = {...product};
        changedProduct["business"] = business;
        setProduct(changedProduct);
        console.log(product)

    }, [business])


    return (
        <Page>

            <Container className={"white-background padding"}>
                <Form className={"new-form"} onSubmit={handleSubmit}>
                    {title}

                    <div className={"white-background"}>

                        <label className="btn btn-default">
                            <input type="file" onChange={selectFile}/>
                        </label>

                        {currentFile && (
                            <div className="progress">
                                <div
                                    className="progress-bar progress-bar-info progress-bar-striped"
                                    role="progressbar"
                                    aria-valuenow={progress}
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                    style={{width: progress + "%"}}>
                                    {progress}%
                                </div>
                            </div>
                        )}

                        <br/>
                        <div className="card">
                            <div className="card-header">Product image</div>

                            <div className="alert-light" role="alert">
                                {message}
                            </div>
                            {product.image && <div>
                                <br/>

                                <img src={product.image.url} alt={"Product Image"}/>
                                <br/>
                                <button className={"button-medium"} onClick={(e) => {
                                    e.preventDefault();
                                    setSelectedFiles(null);
                                }}>Delete image
                                </button>
                            </div>
                            }
                            {!product.image && <img src={photo} alt={"Profile Image"}/>}

                        </div>
                    </div>

                    <br/>

                    <FormGroup>
                        <Label for="name">Product name</Label>
                        <Input type="text" name="name" id="name" value={product.name || ''}
                               onChange={handleChange} autoComplete="name"/>
                    </FormGroup>

                    <FormGroup>
                        <Label for="description">Product description</Label>
                        <Input type="textarea" name="description" id="description" value={product.description || ''}
                               onChange={handleChange} autoComplete="description"/>
                    </FormGroup>


                    <FormGroup>
                        <Label for="price">Price</Label>
                        <Input type="double" name="price" id="price" value={product.price || ''}
                               onChange={handleChange}/>
                    </FormGroup>

                    <FormGroup>
                        <Label for="currency">Currency</Label>
                        <Input type="text" name="currency" id="currency" value={product.currency || ''}
                               onChange={handleChange} autoComplete="currency"/>
                    </FormGroup>

                    <div>
                        <button className={"button-primary"} type={"submit"}>Save</button>
                        <button className={"button-secondary"} onClick={handleCancel}>Cancel</button>
                        <br/>
                    </div>

                </Form>
            </Container>
        </Page>);
}