import React, {useState, useRef} from "react";
import {useDispatch, useSelector} from "react-redux";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import {appointmentService, postAppointment} from "../actions/appointment";
import {Page} from "./components";

const required = (value) => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

const AddAppointment = () => {
    const {user: currentUser} = useSelector((state) => state.auth);
    const form = useRef();
    const checkBtn = useRef();
    const [successful, setSuccessful] = useState(false);

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);

    const [hours, setHours] = useState(0);
    const [minutes, setMinutes] = useState(0);

    const businessId = currentUser.employee.business.id;

    const {message} = useSelector(state => state.message);
    const dispatch = useDispatch();

    const onChangeName = (e) => {
        const name = e.target.value;
        setName(name);
    };

    const onChangeDescription = (e) => {
        const description = e.target.value;
        setDescription(description);
    };

    const onChangePrice = (e) => {
        const price = e.target.value;
        setPrice(price);
    };


    const handleSubmit = (e) => {
        e.preventDefault();

        setSuccessful(false);

        form.current.validateAll();

        if (checkBtn.current.context._errors.length === 0) {
            const duration = parseInt(hours) * 60 + parseInt(minutes);

            postAppointment(businessId, name, description, price, duration)
                .then(() => {
                    setSuccessful(true);
                })
                .catch(() => {
                    setSuccessful(false);
                });
        }
    };

    return (
        <Page>

            <Form onSubmit={handleSubmit} className="col-lg-6 offset-lg-3 " ref={form}>
                {!successful && (
                    <div>
                        <div className="form-group">
                            <label htmlFor="name">Date and time</label>
                            <Input
                                type="datetime-local"
                                className="form-control"
                                name="name"
                                value={name}
                                onChange={onChangeName}
                                validations={[required]}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="description">Staff</label>
                            <Input
                                type="text"
                                className="form-control"
                                name="description"
                                value={description}
                                onChange={onChangeDescription}
                                validations={[required]}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="price">Service</label>
                            <Input
                                type="number"
                                className="form-control"
                                name="price"
                                value={price}
                                onChange={onChangePrice}
                                validations={[required]}
                            />
                        </div>

                        <div className="form-group">
                            <button className="btn btn-primary btn-block">Add</button>
                        </div>
                    </div>
                )}

                {message && (
                    <div className="form-group">
                        <div className={successful ? "alert alert-success" : "alert alert-danger"} role="alert">
                            {message}
                        </div>
                    </div>
                )}
                <CheckButton style={{display: "none"}} ref={checkBtn}/>
            </Form>
        </Page>
    );
};

export default AddAppointment;