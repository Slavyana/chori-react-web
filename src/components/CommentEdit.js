import React, { useEffect, useState} from 'react';
import { Container, Form, FormGroup, Input, Label } from 'reactstrap';

import {Page} from "./components";
import {useSelector} from "react-redux";


export function CommentEdit(props) {

    let handleCancel = (event) => {
        props.history.push('/profile');
    }


    let emptyPost = {
        author: null,
        content: ''
    };

    const { user: currentUser } = useSelector((state) => state.auth);

    const [isLoaded, setIsLoaded] = useState(false);
    const postId = props.match.params.postId;
    const commentId = props.match.params.commentId;

    const [comment, setComment] = useState(emptyPost);
    const [error, setError] = useState(null);

    useEffect(() => {
        fetch(`http://localhost:8080/api/posts/${postId}`)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    console.log(result)
                    console.log(commentId)

                    let c = result.comments.find(comment => comment.id === commentId)
                    // result.comments.map(c =>  console.log(c.id))
                    console.log(c)

                    setComment(c);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])


    const title = <h2>Edit comment</h2>;


    let handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let changedComment = {...comment};
        changedComment[name] = value;
        setComment(changedComment);
    }

    let handleSubmit = (event) => {
        event.preventDefault();
        console.log(comment)

        fetch('http://localhost:8080/api/posts/' + postId + '/comment/' + commentId, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(comment),
        }).then(() => props.history.push('/profile'));
    }


    useEffect(() => {
        let changedComment = {...comment};
        changedComment["author"] = {...currentUser, roles: null};
        setComment(changedComment);

    }, [currentUser])


    return (
        <Page>

            <Container className={"white-background padding"}>
                <Form className={"new-form"} onSubmit={handleSubmit}>
                    {title}

                    <div className={"white-background"}>


                    <FormGroup>
                        <Label for="description">Comment</Label>
                        <Input type="textarea" name="content" id="content" value={comment.content || ''}
                               onChange={handleChange} autoComplete="content"/>
                    </FormGroup>

                    <div>
                        <button className={"button-primary"} type={"submit"}>Save</button>
                        <button className={"button-secondary"} onClick={handleCancel}>Cancel</button>
                        <br/>
                    </div>
                    </div>

                </Form>
            </Container>
        </Page>);
}