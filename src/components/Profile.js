import React, {useEffect, useState} from "react";
import {Link, Redirect} from 'react-router-dom';
import {useSelector} from "react-redux";
import {
    getBusiness,
    getBusinessEmployees,
    getBusinessPosts,
    getBusinessProducts,
    getBusinessServices
} from "../actions/business";
import {getEmployee} from "../actions/employee";
import {Button, FormGroup, Input, Label} from "reactstrap";
import {Page} from "./components";


import photo from "../static/images/photo-icon.png"
import {Form} from "react-bootstrap";

const useScript = url => {
    useEffect(() => {
        const script = document.createElement('script');
        script.src = url;
        script.async = true;
        document.body.appendChild(script);
    }, [url]);
};

const Profile = (props) => {
    const {user: currentUser} = useSelector((state) => state.auth);
    const [business, setBusiness] = useState({});
    const [employee, setEmployee] = useState({});
    const [services, setServices] = useState([]);
    const [employees, setEmployees] = useState([]);
    const [posts, setPosts] = useState([]);
    const [products, setProducts] = useState([]);
    const [commentContent, setCommentContent] = useState([]);


    let handleLike = (id) => {
        console.log(id);
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + id + '/like', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify(currentUser),
        }).then(() => props.history.push('/profile'));
    }

    let handleUnlike = (id) => {
        console.log(id);
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + id + '/unlike/' + currentUser.id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then(() => props.history.push('/profile'));
    }


    let handleCommentLike = (postId, commentId) => {
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + postId + '/' + commentId + '/like', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify(currentUser),
        }).then(() => props.history.push('/profile'));
    }

    let handleCommentUnlike = (postId, commentId) => {
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + postId + '/' + commentId + '/unlike/' + currentUser.id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then(() => props.history.push('/profile'));
    }


    let handleComment = (id) => {
        console.log(id);
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        let comment = {
            author: currentUser,
            content: commentContent
        }

        fetch('http://localhost:8080/api/posts/' + id + '/comment/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify(comment),

        }).then(() => props.history.push('/profile'));
    }


    useScript('jquery.js');
    useScript('nicepage.js');


    useEffect(() => {
        getBusiness(currentUser.employee.business.id)
            .then((response) => {
                setBusiness(response);
            });

        getEmployee(currentUser.employee.id)
            .then((response) => {
                setEmployee(response);
            });

        getBusinessServices(currentUser.employee.business.id)
            .then((response) => {
                setServices(response);
            });

        getBusinessEmployees(currentUser.employee.business.id)
            .then((response) => {
                setEmployees(response);
            });

        getBusinessPosts(currentUser.employee.business.id)
            .then((response) => {
                setPosts(response);
            });

        getBusinessProducts(currentUser.employee.business.id)
            .then((response) => {
                setProducts(response);
            });
    }, [])

    if (!currentUser) {
        return <Redirect to="/"/>;
    }

    const serviceList = services.map(service => {
        return <div>
            <ul>
                <li>{service.name}</li>
                <li>{service.description}</li>
                <li>Price: {service.price} GBP</li>
                <li>Duration (minutes): {service.durationInMinutes}</li>
                {<Button tag={Link} to={"/profile/services/" + service.id} id={service.id}>Edit</Button>}
            </ul>

        </div>
    });

    const employeeList = employees.map(employee => {
        return <div>
            <ul>
                <li key={employee.id}> {employee.firstName} {employee.lastName}</li>
                {(employee.id === currentUser.employee.id || currentUser.roles && currentUser.roles.includes("ROLE_MANAGER")) &&
                <Button tag={Link} to={"/profile/employees/" + employee.id} id={employee.id}>Edit</Button>}
            </ul>

        </div>
    });

    const postsList = posts.map(post => {
        return <div>
            <ul>
                <li>{post.image.url}</li>
                <li>{post.description}</li>
                <li>{post.business.username}</li>

                Liked by:
                {post.likes.map(like => {
                    return <li>{like.username}</li>
                })}

                <button onClick={() => handleLike(post.id)}>Like</button>
                <button onClick={() => handleUnlike(post.id)}>Unlike</button>


                Comments:
                {post.comments.map(comment => {
                    return (
                        <div>
                            <li>{comment.content}</li>
                            <button onClick={() => handleCommentLike(post.id, comment.id)}>Like</button>
                            <button onClick={() => handleCommentUnlike(post.id, comment.id)}>Unlike</button>

                        </div>
                    )
                })}
                <Form>
                    <FormGroup>
                        <Label for="description">Add a comment</Label>
                        <Input type="textarea" name="description" id="description" value={commentContent || ''}
                               onChange={(e) => {
                                   setCommentContent(e.target.value)
                               }} autoComplete="name"/>
                        <button onClick={() => handleComment(post.id)}>Add Comment</button>
                    </FormGroup>

                </Form>


                {/*{<Button tag={Link} to={"/profile/services/" + service.id} id={service.id}>Edit</Button>}*/}
            </ul>
        </div>
    });

    const productsList = products.map(product => {
        return <div>
            <ul>
                <li>{product.image && product.image.url}</li>
                <li>{product.name}</li>
                <li>{product.description}</li>
                {/*{<Button tag={Link} to={"/profile/services/" + service.id} id={service.id}>Edit</Button>}*/}
            </ul>
        </div>
    });

    return (
        <Page>
            <div className="container">
                <header className="jumbotron">
                    <h3>
                        <strong>{currentUser.username}</strong> Profile
                    </h3>
                    <p>
                        <strong>Email:</strong> {currentUser.email}
                    </p>
                </header>
                <strong>Current employee:</strong>
                <ul>
                    <li> {employee.firstName} {employee.lastName}</li>
                    <li> {employee.lastName}</li>
                </ul>

                <strong>Business:</strong>
                <ul>
                    {business.profileImage && <img src={business.profileImage.url}/>}
                    {!business.profileImage && <img src={photo}/>}

                    <li> {business.name}</li>
                    <li> {business.country}</li>
                    <li> {business.region}</li>
                    <li> {business.address}</li>
                    <li> {business.username}</li>

                    <li> {business.description}</li>

                    <Button tag={Link} to={"/profile/business/" + business.id} id={business.id}>Edit</Button>
                </ul>


                <strong>Services:</strong>
                {serviceList}

                <strong>Employees:</strong>
                {employeeList}

                <strong>Posts:</strong>
                {postsList}

                <strong>Products:</strong>
                {productsList}

            </div>
        </Page>
    )

};


export default Profile;





