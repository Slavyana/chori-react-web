import React, {useEffect} from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";


export function Reviews() {



    return(
        <div className="main-section">

        <div className="rating-part">
            <div className="reviews"><h1>Reviews</h1></div>
            <div className="comment-part">
                <div className="user-img-part">
                    <div className="user-img">
                        <img src="/demo/man01.png"/>
                    </div>
                    <div className="user-text">
                        <p>Tom kok</p>
                        <span>Report</span>

                    </div>
                    <div style={{clear: 'both'}}></div>

                </div>
                <div class="comment">
                    <i aria-hidden="true" class="fa fa-star"></i>
                    <i aria-hidden="true" class="fa fa-star"></i>
                    <i aria-hidden="true" class="fa fa-star"></i>
                    <i aria-hidden="true" class="fa fa-star"></i>
                    <i aria-hidden="true" class="fa fa-star-o"></i>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco.</p>
                </div>
                <div style={{clear: 'both'}}></div>

            </div>
            <div class="comment-part">
                <div class="user-img-part">
                    <div class="user-img">
                        <img src="/demo/man02.png"/>
                    </div>
                    <div class="user-text">
                        <p>Win Rool</p>
                        <span>Report</span>
                    </div>
                    <div style={{clear: 'both'}}></div>

                </div>
                <div class="comment">
                    <i aria-hidden="true" class="fa fa-star"></i>
                    <i aria-hidden="true" class="fa fa-star"></i>
                    <i aria-hidden="true" class="fa fa-star"></i>
                    <i aria-hidden="true" class="fa fa-star-o"></i>
                    <i aria-hidden="true" class="fa fa-star-o"></i>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco.</p>
                </div>
                <div style={{clear: 'both'}}></div>

            </div>
            <div className="comment-part">
                <div className="user-img-part">
                    <div className="user-img">
                        <img src="/demo/man03.png"/>
                    </div>
                    <div className="user-text">

                        <p>Jui Choal</p>
                        <span>Report</span>
                    </div>
                    <div style={{clear: 'both'}}></div>

                </div>
                <div className="comment">
                    <i aria-hidden="true" class="fa fa-star"></i>
                    <i aria-hidden="true" class="fa fa-star"></i>
                    <i aria-hidden="true" class="fa fa-star-o"></i>
                    <i aria-hidden="true" class="fa fa-star-o"></i>
                    <i aria-hidden="true" class="fa fa-star-o"></i>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco.</p>
                </div>
                <div style={{clear: 'both'}}></div>

            </div>
            <div className="comment-part">
                <div className="user-img-part">
                    <div className="user-img">
                        <img src="/demo/man04.png"/>
                    </div>
                    <div class="user-text">

                        <p>Jack Mins</p>
                        <span>Report</span>
                    </div>
                    <div style={{clear: 'both'}}></div>

                </div>
                <div class="comment">
                    <i aria-hidden="true" class="fa fa-star"></i>
                    <i aria-hidden="true" class="fa fa-star-o"></i>
                    <i aria-hidden="true" class="fa fa-star-o"></i>
                    <i aria-hidden="true" class="fa fa-star-o"></i>
                    <i aria-hidden="true" class="fa fa-star-o"></i>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco.</p>
                </div>
                <div style={{clear: 'both'}}></div>
            </div>
        </div>
        </div>
            );
}

export default function SimpleRating() {
    const [value, setValue] = React.useState(2);

    return (
        <div>
            <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Controlled</Typography>
                <Rating
                    name="simple-controlled"
                    value={value}
                    onChange={(event, newValue) => {
                        setValue(newValue);
                    }}
                />
            </Box>
            <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Read only</Typography>
                <Rating name="read-only" value={value} readOnly />
            </Box>
            <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Disabled</Typography>
                <Rating name="disabled" value={value} disabled />
            </Box>
            <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Pristine</Typography>
                <Rating name="pristine" value={null} />
            </Box>
        </div>
    );
}



function Stars() {
    const [value, setValue] = React.useState(2);

    return (
        <div>
            <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Controlled</Typography>
                <Rating
                    name="simple-controlled"
                    value={value}
                    onChange={(event, newValue) => {
                        setValue(newValue);
                    }}
                />
            </Box>
            <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Read only</Typography>
                <Rating name="read-only" value={value} readOnly />
            </Box>
            <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Disabled</Typography>
                <Rating name="disabled" value={value} disabled />
            </Box>
            <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Pristine</Typography>
                <Rating name="pristine" value={null} />
            </Box>
        </div>
    );
}