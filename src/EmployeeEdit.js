import React, {useEffect, useState} from 'react';
import {Container, Form, FormGroup, Input, Label} from 'reactstrap';
import {Page} from "./components/components";
import Select from "react-select";
import {getBusinessServices} from "./actions/business";
import photo from "./static/images/photo-icon.png";
import UploadService from "./services/upload-files.service";

let options = []


export function EmployeeEdit(props) {

    let emptyEmployee = {
        firstName: '',
        lastName: '',
        services: [],
        profileImage: {}
    };

    const [isLoaded, setIsLoaded] = useState(false);
    const id = props.match.params.id;

    const [employee, setEmployee] = useState(emptyEmployee);
    const [error, setError] = useState(null);
    const [services, setServices] = useState([]);
    const [selectedOption, setSelectedOption] = useState([]);
    const [fileInfos, setFileInfos] = useState([]);
    const [message, setMessage] = useState("");
    const [progress, setProgress] = useState(0);
    const [currentFile, setCurrentFile] = useState(undefined);
    const [selectedFiles, setSelectedFiles] = useState(undefined);

    const selectFile = (event) => {
        setSelectedFiles(event.target.files);
    };

    const upload = () => {
        let currentFile = selectedFiles[0];

        setProgress(0);
        setCurrentFile(currentFile);

        UploadService.upload(currentFile, (event) => {
            setProgress(Math.round((100 * event.loaded) / event.total));
        })
            .then((response) => {
                setMessage(response.data.message);
                return UploadService.getFiles();
            })
            .then((files) => {

                let changedEmployee = {...employee};

                let file = files.data.slice(-1)[0]
                changedEmployee["profileImage"] = file;

                setEmployee(changedEmployee);

                setFileInfos(files.data);
            })
            .catch(() => {
                setProgress(0);
                setMessage("Could not upload the file!");
                setCurrentFile(undefined);
            });

        setSelectedFiles(undefined);
    };


    let handleCancel = (event) => {
        props.history.push('/profile');
    }

    useEffect(() => {
        options = []
        if (employee.business) {
            getBusinessServices(employee.business.id)
                .then((response) => {
                    setServices(response);
                });
        }

    }, [employee])

    useEffect(() => {
        services.map((service => options.push({value: service.id, label: service.name})))

    }, [services])

    useEffect(() => {
        let changedEmployee = {...employee};
        let arr = [];
        selectedOption.map(option => arr.push(option.value));
        changedEmployee["services"] = arr;
        setEmployee(changedEmployee);
    }, [selectedOption])

    useEffect(() => {
        if (selectedFiles) {
            upload();
        } else {
            let changedEmployee = {...employee};
            changedEmployee["profileImage"] = null;
            setEmployee(changedEmployee);
        }
    }, [selectedFiles])


    useEffect(() => {
        fetch(`http://localhost:8080/api/employees/${id}`)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setEmployee(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])


    useEffect(() => {
        fetch(`http://localhost:8080/api/employees/${id}/services`)
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result)

                    if (result) {
                        console.log(result)
                        result.map(s => selectedOption.push({value: s.id, label: s.name}))

                    }

                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [id])

    const title = <h2>{employee.id ? 'Edit Employee' : 'Add Employee'}</h2>;


    let handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let changedEmployee = {...employee};
        changedEmployee[name] = value;
        setEmployee(changedEmployee);
    }

    let handleSubmit = (event) => {
        event.preventDefault();

        fetch('http://localhost:8080/api/employees' + (employee.id ? '/' + id : ''), {
            method: (employee.id) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(employee),
        }).then(() => props.history.push('/profile'));
    }

    return (
        <div>

            <Page>
                <Container className={"white-background padding"}>

                    <Form className={"new-form"} onSubmit={handleSubmit}>
                        {title}

                        <div className={"white-background"}>

                            <label className="btn btn-default">
                                <input type="file" onChange={selectFile}/>
                            </label>

                            {currentFile && (
                                <div className="progress">
                                    <div
                                        className="progress-bar progress-bar-info progress-bar-striped"
                                        role="progressbar"
                                        aria-valuenow={progress}
                                        aria-valuemin="0"
                                        aria-valuemax="100"
                                        style={{width: progress + "%"}}>
                                        {progress}%
                                    </div>
                                </div>
                            )}

                            <br/>
                            <div className="card">
                                <div className="card-header">Image</div>

                                <div className="alert-light" role="alert">
                                    {message}
                                </div>
                                {employee.profileImage && <div>
                                    <br/>

                                    <img src={employee.profileImage && employee.profileImage.url} alt={"Post Image"}/>
                                    <br/>
                                    <button className={"button-medium"} onClick={(e) => {
                                        e.preventDefault();
                                        setSelectedFiles(null);
                                    }}>Delete image
                                    </button>
                                </div>
                                }
                                {!employee.profileImage && <img src={photo} alt={"Profile Image"}/>}

                            </div>
                        </div>

                        <br/>

                        <FormGroup>
                            <Label for="firstName">First name</Label>
                            <Input type="text" name="firstName" id="firstName" value={employee.firstName || ''}
                                   onChange={handleChange} autoComplete="firstName"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="lastName">Last name</Label>
                            <Input type="lastName" name="lastName" id="lastName" value={employee.lastName || ''}
                                   onChange={handleChange} autoComplete="lastName"/>
                        </FormGroup>

                        <FormGroup>
                            <Label for="services">Services</Label>
                            <Select
                                isMulti
                                name=""
                                options={options}
                                defaultValue={selectedOption}
                                onChange={setSelectedOption}
                                className="basic-multi-select"
                                classNamePrefix="select"
                            />
                        </FormGroup>

                        <div>
                            <button className={"button-primary"} type={"submit"}>Save</button>
                            <button className={"button-secondary"} onClick={handleCancel}>Cancel</button>
                            <br/>
                        </div>
                    </Form>
                </Container>
            </Page>
        </div>);
}