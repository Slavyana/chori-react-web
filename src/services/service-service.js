import axios from "axios";

const API_URL = "http://localhost:8080/api/services/";

const postService = (businessId, name, description, price, currency, duration) => {

    const postData = {
        name: name,
        description: description,
        price: price,
        currency: currency,
        business: businessId,
        durationInMinutes: duration
    }

    return axios.post(API_URL, postData);
};

export default {
    postService
};