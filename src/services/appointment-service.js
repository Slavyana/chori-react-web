import axios from "axios";

const API_URL = "http://localhost:8080/api/appointments/";

const postAppointment = (businessId, name, description, price, duration) => {

    const postData = {
        name: name,
        description: description,
        price: price,
        business: businessId,
        durationInMinutes: duration
    }


    return axios.post(API_URL, postData);
};

export default {
    postAppointment
};