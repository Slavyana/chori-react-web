import axios from "axios";


const API_URL = "http://localhost:8080/api/businesses/";

const getBusiness = (id) => {
    return axios
        .get(API_URL + id)
        .then((response) => {
            return response.data;
        });
};

const getBusinessServices = (id) => {
    return axios
        .get(API_URL + id + "/services")
        .then((response) => {
            return response.data;
        });
};

const getBusinessEmployees = (id) => {
    return axios
        .get(API_URL + id + "/employees")
        .then((response) => {
            return response.data;
        });
};

const getBusinessPosts = (id) => {
    return axios
        .get(API_URL + id + "/posts")
        .then((response) => {
            return response.data;
        });
};


const getBusinessProducts = (id) => {
    return axios
        .get(API_URL + id + "/products")
        .then((response) => {
            return response.data;
        });
};

export default {
    getBusiness,
    getBusinessServices,
    getBusinessEmployees,
    getBusinessPosts,
    getBusinessProducts
};