import axios from "axios";

const API_URL = "http://localhost:8080/api/auth/";

const register = (username, email, password, firstName, lastName, phoneNumber, dateOfBirth, businessUsername, businessName, businessCountry, businessRegion, businessAddress, roles, businessId,) => {

    const postData = {
        email: email,
        username: username,
        password: password,
        employee: {
            username: username,
            firstName: firstName,
            lastName: lastName,
            dateOfBirth: dateOfBirth,
            phoneNumber: phoneNumber
        },
        client: {
            username: username,
            firstName: firstName,
            lastName: lastName,
            dateOfBirth: dateOfBirth,
            phoneNumber: phoneNumber
        },
        business: {
            id: businessId,
            username: businessUsername,
            name: businessName,
            country: businessCountry,
            region: businessRegion,
            address: businessAddress
        },
        roles: roles
    }

    return axios.post(API_URL + "signup", postData);
};

const login = (username, password) => {
    return axios
        .post(API_URL + "signin", {
            username,
            password,
        })
        .then((response) => {
            if (response.data.accessToken) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }
            return response.data;
        });
};

const logout = () => {
    localStorage.removeItem("user");
};

export default {
    register,
    login,
    logout,
};