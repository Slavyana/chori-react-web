import axios from "axios";


const API_URL = "http://localhost:8080/api/clients/";

const getClient = (id) => {
    return axios
        .get(API_URL + id)
        .then((response) => {
            return response.data;
        });
};


export default {
    getClient
};