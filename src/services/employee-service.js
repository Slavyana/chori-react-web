import axios from "axios";


const API_URL = "http://localhost:8080/api/employees/";

const getEmployee = (id) => {
    return axios
        .get(API_URL + id)
        .then((response) => {
            return response.data;
        });
};


export default {
    getEmployee
};