import React from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faChartPie, faGlobe, faCog, faUsers} from '@fortawesome/free-solid-svg-icons'
import info1 from "../static/images/info-box-img-1.jpeg"
import info2 from "../static/images/info-box-img-2.jpeg"

export function GridPage() {
    return <React.Fragment>
        {/* Wrapper */}
        <div className={"wrapper"}>

            {/* Navigation */}
            <nav className={"main-nav"}>
                <ul>
                    <li><a href={"#"}>Home</a></li>
                    <li><a href={"#"}>About</a></li>
                    <li><a href={"#"}>Services</a></li>
                    <li><a href={"#"}>Contact</a></li>
                </ul>

            </nav>

            {/* Top Container */}
            <section className={"top-container"}>
                <header className={"showcase"}>
                    <h1>Your web presence</h1>
                    <p>dgkjdla adsdahjldk a dadlahskdma
                        asdhjdklad sdsd sadda sadada Lorem ipsum more words</p>
                    <a href={"#"} className={"btn"}>Read more</a>
                </header>
                <div className={"top-box top-box-a"}>
                    <h4>Membership</h4>
                    <p className={"price"}>£199/mo</p>
                    <a href={"#"} className={"btn"}>Buy now </a>
                </div>
                <div className={"top-box top-box-b"}>
                    <h4>Pro Membership</h4>
                    <p className={"price"}>£299/mo</p>
                    <a href={"#"} className={"btn"}>Buy now </a>
                </div>
            </section>

            {/* Boxes Section */}
            <section className={"boxes"}>
                <div className={"box"}>
                    <FontAwesomeIcon icon={faChartPie} size={"4x"}/>
                    <h3>Analytics</h3>
                    <p>10 words here to see what a paragraph looks like here are some more to make it longer</p>
                </div>
                <div className={"box"}>
                    <FontAwesomeIcon icon={faGlobe} size={"4x"}/>
                    <h3>Marketing</h3>
                    <p>10 words here to see what a paragraph looks like here are some more to make it longer</p>
                </div>
                <div className={"box"}>
                    <FontAwesomeIcon icon={faCog} size={"4x"}/>
                    <h3>Development</h3>
                    <p>10 words here to see what a paragraph looks like here are some more to make it longer</p>
                </div>
                <div className={"box"}>
                    <FontAwesomeIcon icon={faUsers} size={"4x"}/>
                    <h3>Analytics</h3>
                    <p>10 words here to see what a paragraph looks like here are some more to make it longer</p>
                </div>
            </section>


            {/* Info Section */}
            <section className={"info"}>
                <img src={info1}/>
                <div>
                    <h2>Your business on the web</h2>
                    <p>A long text here but I am too lazy to copy something
                        from the web so I am just typing some words here,
                        I suppose that works just as well,
                        even better. Should I make it even bigger, I guess I'll try</p>

                    <a href={"#"} className={"btn"}>Learn More</a>
                </div>


            </section>


            {/* Portfolio */}
            <section className={"portfolio"}>
                <img src="https://picsum.photos/200"/>
                <img src="https://picsum.photos/201"/>
                <img src="https://picsum.photos/202"/>
                <img src="https://picsum.photos/203"/>
                <img src="https://picsum.photos/204"/>
                <img src="https://picsum.photos/205"/>
                <img src="https://picsum.photos/206"/>
                <img src="https://picsum.photos/207"/>
                <img src="https://picsum.photos/208"/>
            </section>


            {/* Footer */}
            <footer>
                <p>Grid &copy; 2020</p>
            </footer>


        </div>
        {/* Wrapper Ends*/}


    </React.Fragment>


}