import React, {useEffect, useState} from "react";

import {Link} from "react-router-dom";
import {Container} from "reactstrap";
import queryString from 'query-string';
import photo from "../static/images/photo-icon.png";
import {Page} from "../components/components";

function BookingPageSearchForm() {
    return <form>
        <input type={"text"} value={"Leeds"}/>
        <input type={"date"} placeholder={"Any date"}/>
        <input type={"time"} placeholder={"Any time"}/>
        <input type={"textarea"} placeholder={"All services"}/>
        <button type={"submit"}>Search</button>
    </form>

}

export function BookingPage(props) {
    let params = queryString.parse(props.location.search)
    console.log(params);

    const [businesses, setBusinesses] = useState([]);

    useEffect(() => {
        fetch(`http://localhost:8080/api/businesses?country=${params["country"] || ''}&region=${params["region"] || ''}&serviceName=${params["service"] || ''}`)
            .then(response => response.json())
            .then(data => {
                setBusinesses(data)
            });
    }, [])

    let businessList = businesses.map(b => {
        return <div className="business-card">
            <div>
                <div className="horizontal-card">

                    <div>
                        {b.profileImage &&
                        <img className={"profileImage"} src={b.profileImage.url}/>}
                        {!b.profileImage && <img src={photo} alt={"none"}/>}
                    </div>
                    <div className="horizontal-card-body">
                        <span> </span>
                        <h4 className="card-title">{b.name}</h4>
                        <span className="card-text"><Link to={`profile-${b.username}`}>@{b.username}</Link></span>

                    </div>
                    <div className="horizontal-card-footer">
                        <form action={`profile-${b.username}`}>
                            <button>Visit profile</button>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    })

    return <Page>
        <Container className={"white-background padding container auto"}>
            <div>
                {businessList}
            </div>
        </Container>
    </Page>
}