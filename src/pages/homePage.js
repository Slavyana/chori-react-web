import React, {useState} from "react";
import {Carousel} from "react-bootstrap";
import {HomeBookingForm} from "../components/forms";
import {Page} from "../components/components";
import {useSelector} from "react-redux";

// Controlled Carousel
function ControlledCarousel() {
    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };

    return (
        <div className={"carousel"}>
            <Carousel controls={false} activeIndex={index} onSelect={handleSelect} interval={5000}>
                <Carousel.Item className={"carousel-item carousel-img-1"}>
                </Carousel.Item>
                <Carousel.Item className={"carousel-item carousel-img-2"}>
                </Carousel.Item>
            </Carousel>
        </div>
    );
}

// Showcase
function Showcase() {
    const {user: currentUser} = useSelector((state) => state.auth);
    return (<div className={"showcase-wrapper"}>
            <ControlledCarousel/>
            {currentUser && !currentUser.employee &&
            <HomeBookingForm/>}
        </div>

    )
}

// Info box container
export function InfoContainer() {
    return <div className={"info-container"}>
        <InfoBox nextPage={"/terms"} titleText={"Your business online"} buttonText={"Learn more"}
                 className={"info-box-img info-box-img-1"}>
            <p>Register, advertise and expand <br/> your business</p></InfoBox>
        <InfoBox nextPage={"/search"} titleText={"The right choice"} buttonText={"Book an appointment"}
                 className={"info-box-img info-box-img-2"}><p>Choose from hundreds of experts <br/> wherever you are</p>
        </InfoBox>
        <InfoBox nextPage={"/discover"} titleText={"Aspire to inspire"} buttonText={"Discover"}
                 className={"info-box-img info-box-img-3"}>
            <p>Find inspiration for your next look <br/> and showcase your style</p></InfoBox>
        <InfoBox nextPage={"/discover"} titleText={"Be fabulous"} buttonText={"See more"}
                 className={"info-box-img info-box-img-4"}><p>Save and
            share your most fabulous <br/> moments with us</p></InfoBox>
    </div>
}

// Info Box
function InfoBox(props) {
    const {className, buttonText, titleText, nextPage} = props
    return <div className={"info-box"}>
        <div className={className}/>
        <h4>{titleText}</h4>
        {props.children}
    </div>
}

export function HomePage(props) {
    const {history} = props
    return (
        <Page history={history}>
            <Showcase/>
            <InfoContainer/>
        </Page>
    );
}