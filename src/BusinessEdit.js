import React, {useEffect, useState} from 'react';
import {Container, Form, FormGroup, Input, Label} from 'reactstrap';
import {Page} from "./components/components";
import {CountryDropdown, RegionDropdown} from "react-country-region-selector";
import UploadService from "./services/upload-files.service";
import photo from "./static/images/photo-icon.png"

export function BusinessEdit(props) {

    let emptyBusiness = {
        profileImage: null,
        name: '',
        username: '',
        description: '',
        address: '',
        country: '',
        region: '',
    };

    const [isLoaded, setIsLoaded] = useState(false);
    const id = props.match.params.id;
    const [business, setBusiness] = useState(emptyBusiness);
    const [error, setError] = useState(null);

    const [selectedFiles, setSelectedFiles] = useState(undefined);
    const [currentFile, setCurrentFile] = useState(undefined);
    const [progress, setProgress] = useState(0);
    const [message, setMessage] = useState("");

    const [fileInfos, setFileInfos] = useState([]);

    const selectFile = (event) => {
        setSelectedFiles(event.target.files);
    };

    const upload = () => {
        let currentFile = selectedFiles[0];

        setProgress(0);
        setCurrentFile(currentFile);

        UploadService.upload(currentFile, (event) => {
            setProgress(Math.round((100 * event.loaded) / event.total));
        })
            .then((response) => {
                console.log(response)
                setMessage(response.data.message);
                return UploadService.getFiles();
            })
            .then((files) => {

                let changedBusiness = {...business};

                let file = files.data.slice(-1)[0]
                changedBusiness["profileImage"] = file;

                setBusiness(changedBusiness);

                setFileInfos(files.data);
            })
            .catch(() => {
                setProgress(0);
                setMessage("Could not upload the file!");
                setCurrentFile(undefined);
            });

        setSelectedFiles(undefined);
    };

    useEffect(() => {
        UploadService.getFiles().then((response) => {
            setFileInfos(response.data);
        });
    }, []);

    useEffect(() => {
        fetch(`http://localhost:8080/api/businesses/${id}`)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setBusiness(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    const title = <h2>{business.id ? 'Edit business profile' : 'Add business'}</h2>;


    let handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let changedBusiness = {...business};
        changedBusiness[name] = value;
        setBusiness(changedBusiness);
    }

    let handleSubmit = (event) => {
        event.preventDefault();
        fetch('http://localhost:8080/api/businesses' + (business.id ? '/' + id : ''), {
            method: (business.id) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(business),
        }).then(() => props.history.push('/profile'));
    }

    let handleCancel = (event) => {
        props.history.push('/profile');
    }

    useEffect(() => {
        if (selectedFiles) {
            upload();
        } else {
            let changedBusiness = {...business};
            changedBusiness["profileImage"] = null;
            setBusiness(changedBusiness);
        }
    }, [selectedFiles])

    return (
        <Page>

            <Container className={"white-background padding"}>
                <Form className={"new-form"} onSubmit={handleSubmit}>
                    {title}

                    <div className={"white-background"}>

                        <label className="btn btn-default">
                            <input type="file" onChange={selectFile}/>
                        </label>

                        {currentFile && (
                            <div className="progress">
                                <div
                                    className="progress-bar progress-bar-info progress-bar-striped"
                                    role="progressbar"
                                    aria-valuenow={progress}
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                    style={{width: progress + "%"}}>
                                    {progress}%
                                </div>
                            </div>
                        )}

                        <br/>
                        <div className="card">
                            <div className="card-header">Profile image</div>

                            <div className="alert-light" role="alert">
                                {message}
                            </div>
                            {business.profileImage && <div>
                                <br/>

                                <img src={business.profileImage.url} alt={"Profile Image"}/>
                                <br/>
                                <button className={"button-medium"} onClick={(e) => {
                                    e.preventDefault();
                                    setSelectedFiles(null);
                                }}>Delete image
                                </button>
                            </div>
                            }
                            {!business.profileImage && <img src={photo} alt={"Profile Image"}/>}

                        </div>
                    </div>

                    <br/>

                    <FormGroup>
                        <Label for="name">Business name</Label>
                        <Input type="text" name="name" id="name" value={business.name || ''}
                               onChange={handleChange} autoComplete="name"/>
                    </FormGroup>

                    <FormGroup>
                        <Label for="description">Business description</Label>
                        <Input type="textarea" name="description" id="description" value={business.description || ''}
                               onChange={handleChange} autoComplete="name"/>
                    </FormGroup>


                    <FormGroup>
                        <Label for="address">Address</Label>
                        <Input type="text" name="address" id="address" value={business.address || ''}
                               onChange={handleChange} autoComplete="address-level1"/>
                    </FormGroup>

                    Business country
                    <CountryDropdown
                        className={"select"}
                        value={business.country || ''}
                        onChange={(val) => {
                            let changedBusiness = {...business};
                            changedBusiness["country"] = val;
                            setBusiness(changedBusiness);
                        }}/>

                    Business region
                    <RegionDropdown
                        className={"select"}
                        country={business.country}
                        value={business.region || ''}
                        onChange={(val) => {
                            let changedBusiness = {...business};
                            changedBusiness["region"] = val;
                            setBusiness(changedBusiness);
                        }}/>

                    <div>
                        <button className={"button-primary"} type={"submit"}>Save</button>
                        <button className={"button-secondary"} onClick={handleCancel}>Cancel</button>
                        <br/>
                    </div>

                </Form>
            </Container>
        </Page>);
}