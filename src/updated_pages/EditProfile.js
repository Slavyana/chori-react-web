import React, {useEffect, useState} from 'react';
import {Container, Form, FormGroup, Input, Label} from 'reactstrap';
import {Page} from "../components/components";
import {CountryDropdown, RegionDropdown} from "react-country-region-selector";
import UploadService from "../services/upload-files.service";
import photo from "../static/images/photo-icon.png"

export function ClientEdit(props) {

    let emptyClient = {
        profileImage: null,
        firstName: '',
        lastName: '',
        phoneNumber: '',
        bio: '',
    };

    const [isLoaded, setIsLoaded] = useState(false);
    const id = props.match.params.id;
    const [client, setClient] = useState(emptyClient);
    const [error, setError] = useState(null);

    const [selectedFiles, setSelectedFiles] = useState(undefined);
    const [currentFile, setCurrentFile] = useState(undefined);
    const [progress, setProgress] = useState(0);
    const [message, setMessage] = useState("");

    const [fileInfos, setFileInfos] = useState([]);

    const selectFile = (event) => {
        setSelectedFiles(event.target.files);
    };

    const upload = () => {
        let currentFile = selectedFiles[0];

        setProgress(0);
        setCurrentFile(currentFile);

        UploadService.upload(currentFile, (event) => {
            setProgress(Math.round((100 * event.loaded) / event.total));
        })
            .then((response) => {
                console.log(response)
                setMessage(response.data.message);
                return UploadService.getFiles();
            })
            .then((files) => {

                let changedClient = {...client};

                let file = files.data.slice(-1)[0]
                changedClient["profileImage"] = file;

                setClient(changedClient);

                setFileInfos(files.data);
            })
            .catch(() => {
                setProgress(0);
                setMessage("Could not upload the file!");
                setCurrentFile(undefined);
            });

        setSelectedFiles(undefined);
    };

    useEffect(() => {
        UploadService.getFiles().then((response) => {
            setFileInfos(response.data);
        });
    }, []);

    useEffect(() => {
        fetch(`http://localhost:8080/api/clients/${id}`)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setClient(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    const title = <h2>{client.id ? 'Edit user profile' : 'Add user'}</h2>;


    let handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let changedClient = {...client};
        changedClient[name] = value;
        setClient(changedClient);
    }

    let handleSubmit = (event) => {
        event.preventDefault();
        fetch('http://localhost:8080/api/clients' + (client.id ? '/' + id : ''), {
            method: (client.id) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(client),
        }).then(() => props.history.push('/client-profile'));
    }

    let handleCancel = (event) => {
        props.history.push('/client-profile');
    }

    useEffect(() => {
        if (selectedFiles) {
            upload();
        } else {
            let changedClient = {...client};
            changedClient["profileImage"] = null;
            setClient(changedClient);
        }
    }, [selectedFiles])

    return (
        <Page>

            <Container className={"white-background padding"}>
                <Form className={"new-form"} onSubmit={handleSubmit}>
                    {title}

                    <div className={"white-background"}>

                        <label className="btn btn-default">
                            <input type="file" onChange={selectFile}/>
                        </label>

                        {currentFile && (
                            <div className="progress">
                                <div
                                    className="progress-bar progress-bar-info progress-bar-striped"
                                    role="progressbar"
                                    aria-valuenow={progress}
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                    style={{width: progress + "%"}}>
                                    {progress}%
                                </div>
                            </div>
                        )}

                        <br/>
                        <div className="card">
                            <div className="card-header">Profile image</div>

                            <div className="alert-light" role="alert">
                                {message}
                            </div>
                            {client.profileImage && <div>
                                <br/>

                                <img src={client.profileImage.url} alt={"Profile Image"}/>
                                <br/>
                                <button className={"button-medium"} onClick={(e) => {
                                    e.preventDefault();
                                    setSelectedFiles(null);
                                }}>Delete image
                                </button>
                            </div>
                            }
                            {!client.profileImage && <img src={photo} alt={"Profile Image"}/>}

                        </div>
                    </div>

                    <br/>

                    <FormGroup>
                        <Label for="firstName">First name</Label>
                        <Input type="text" name="firstName" id="firstName" value={client.firstName || ''}
                               onChange={handleChange} autoComplete="firstName"/>
                    </FormGroup>

                    <FormGroup>
                        <Label for="lastName">Last name</Label>
                        <Input type="text" name="lastName" id="lastName" value={client.lastName || ''}
                               onChange={handleChange} autoComplete="lastName"/>
                    </FormGroup>

                    <FormGroup>
                        <Label for="phoneNumber">Phone number</Label>
                        <Input type="text" name="phoneNumber" id="phoneNumber" value={client.phoneNumber || ''}
                               onChange={handleChange} autoComplete="phoneNumber"/>
                    </FormGroup>

                    <FormGroup>
                        <Label for="bio">Bio</Label>
                        <Input type="textarea" name="bio" id="bio" value={client.bio || ''}
                               onChange={handleChange} autoComplete="bio"/>
                    </FormGroup>

                    <div>
                        <button className={"button-primary"} type={"submit"}>Save</button>
                        <button className={"button-secondary"} onClick={handleCancel}>Cancel</button>
                        <br/>
                    </div>

                </Form>
            </Container>
        </Page>);
}