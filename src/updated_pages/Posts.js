import React, {useState, useEffect} from 'react'
import {Container} from "reactstrap";
import User2 from "./img/user2.jpg";
import {Page} from "../components/components";
import {useSelector} from "react-redux";

export function Post(props) {

    const {user: currentUser} = useSelector((state) => state.auth);
    const [isLoaded, setIsLoaded] = useState(false);
    const [posts, setPosts] = useState([]);
    const [error, setError] = useState(null);


    const [commentContent, setCommentContent] = useState([]);

    let handleLike = (id) => {
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + id + '/like', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify(currentUser),
        }).then((response) => {

            response.json().then(data => {
                console.log(data)
                const newList = posts.map((post) => {
                    if (post.id === id) {
                        return data
                    }
                    return post
                });

                setPosts(newList);
            })
        });
    }

    let handleUnlike = (id) => {
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + id + '/unlike/' + currentUser.id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then((response) => {
            response.json().then(data => {
                console.log(data)
                const newList = posts.map((post) => {
                    if (post.id === id) {
                        return data
                    }
                    return post
                });

                setPosts(newList);
            })
        });
    }


    let handleCommentLike = (postId, commentId) => {
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + postId + '/' + commentId + '/like', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify(currentUser),
        }).then((response) => {
            console.log(response)

            response.json().then(data => {
                console.log(data)
                const newList = posts.map((post) => {
                    if (post.id === postId) {
                        return data
                    }
                    return post
                });

                setPosts(newList);
            })
        });
    }

    let handleCommentUnlike = (postId, commentId) => {
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + postId + '/' + commentId + '/unlike/' + currentUser.id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then((response) => {

            response.json().then(data => {
                console.log(data)
                const newList = posts.map((post) => {
                    if (post.id === postId) {
                        return data
                    }
                    return post
                });

                setPosts(newList);
            })
        });
    }


    let handlePostDelete = (postId) => {
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + postId, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then((response) => {
            fetch(`http://localhost:8080/api/posts`)
                .then(res => res.json())
                .then(
                    (result) => {
                        setIsLoaded(true);
                        setPosts(result);
                    },
                    (error) => {
                        setIsLoaded(true);
                        setError(error);
                    }
                )
        });
    }


    let handleCommentDelete = (postId, commentId) => {
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + postId + '/comment/' + commentId, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then((response) => {

            response.json().then(data => {
                console.log(data)
                const newList = posts.map((post) => {
                    if (post.id === postId) {
                        return data
                    }
                    return post
                });

                setPosts(newList);
            })
        });
    }


    let handleComment = (id) => {
        console.log(id);
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        let comment = {
            author: currentUser,
            content: commentContent
        }

        fetch('http://localhost:8080/api/posts/' + id + '/comment/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify(comment),

        }).then((response) => {

            response.json().then(data => {
                console.log(data)
                const newList = posts.map((post) => {
                    if (post.id === id) {
                        return data
                    }
                    return post
                });

                setPosts(newList);
            })
        });
    }

    let handleEditPost = (id) => {
        props.history.push("posts/" + id);
    }

    let handleCommentEdit = (postId, commentId) => {
        props.history.push("posts/" + postId + "/comment/" + commentId);
    }

    let handleProfileVisit = (username) => {
        props.history.push("profile-" + username);
    }


    useEffect(() => {
        fetch(`http://localhost:8080/api/posts`)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setPosts(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])


    const postsList = posts.map(post => {
        return <div className="row">
            <div className="col-sm-6 offset-sm-3">
                <div className="post-block">
                    <div className="d-flex justify-content-between">
                        <div className="d-flex mb-3">
                            <div className="mr-2">
                            </div>
                            <div>
                                <h5 className="mb-0"><a href="#!"
                                                        className="text-dark">{post.author && post.author.username}</a>
                                </h5>
                            </div>
                        </div>
                        <div>
                            {post.author && post.author.id === currentUser.id &&
                            <a href="" onClick={() => handleEditPost(post.id)}
                               className="text-info mr-2"><span>Edit</span></a>}

                            {post.author && post.author.id === currentUser.id &&
                            <a href="" className="text-danger mr-2" onClick={(e) => {
                                e.preventDefault();
                                let result = window.confirm("Are you sure you want to delete this post?");
                                if (result) {
                                    handlePostDelete(post.id)
                                }
                            }}><span>Delete</span></a>}
                        </div>
                    </div>
                    <div className="post-block__content mb-2">
                        <p>{post.description}</p>
                        <img src={post.image && post.image.url} alt="Content img"/>
                    </div>

                    <div>
                        <div>
                            Done by:{post.business &&
                        <a href={""}
                           onClick={() => handleProfileVisit(post.business.username)}> {post.business.name}</a>}
                        </div>
                        <div>
                            <span>Services: </span>
                            {post.services && post.services.map(s => {
                                return s.name
                            })}
                        </div>
                    </div>

                    <hr/>
                    <div className="mb-3">
                        <div className="d-flex justify-content-between mb-2">
                            <div className="d-flex">
                                {!post.likes.find(x => x.id === currentUser.id)
                                && <a href="#!" className="text-danger mr-2" onClick={(e) => {
                                    e.preventDefault();
                                    handleLike(post.id)
                                }}><span>Like</span></a>
                                }
                                {post.likes.find(x => x.id === currentUser.id) &&
                                < a href="#!" className="text-dark mr-2" onClick={(e) => {
                                    e.preventDefault();
                                    handleUnlike(post.id)
                                }}><span>Unlike</span></a>
                                }

                            </div>
                        </div>


                        {post.likes.length > 0 && <p className="mb-0">
                            {post.likes.length} likes

                        </p>}

                    </div>
                    <hr/>


                    <div className="post-block__comments">
                        <div className="input-group mb-3">
                            <input type="text" className="form-control"
                                   placeholder="Add your comment" onChange={(e) => setCommentContent(e.target.value)}/>
                            <div className="input-group-append">
                                <button className="btn btn-primary" type="button"
                                        id="button-addon2" onClick={() => handleComment(post.id)}>Comment
                                </button>
                            </div>
                        </div>

                        <span className="text-dark">Comments ({post.comments.length}):</span>


                        {/* scroll */}
                        <div className="comment-view-box mb-3">
                            {post.comments &&
                            post.comments.map(c => {
                                return (
                                    <div className="d-flex mb-2">

                                        {c.author.employee && c.author.employee.profileImage &&
                                        <img src={c.author.employee.profileImage.url} alt="User img"
                                             className="author-img author-img--small mr-2 mt-3"/>}

                                        {c.author.client && c.author.client.profileImage &&
                                        <img src={c.author.client.profileImage.url} alt="User img"
                                             className="author-img author-img--small mr-2 mt-3"/>}
                                        <div className="d-flex mb-4">

                                            <div>
                                                <h6 className="mb-1"><a href=""
                                                                        onClick={() => handleProfileVisit(c.author.username)}
                                                                        className="text-dark">{c.author.username}</a>
                                                    <span className={"ml-3"}>

                                        {c.author.id === currentUser.id &&
                                        <a href="" className="text-danger mr-2" onClick={(e) => {
                                            e.preventDefault();
                                            let result = window.confirm("Are you sure you want to delete this comment?");
                                            if (result) {
                                                handleCommentDelete(post.id, c.id)
                                            }
                                        }}><span>Delete</span></a>}
                                                        {((c.author.id === currentUser.id) || (post.author === currentUser.id)) &&
                                                        <a href="" className="text-info mr-2" onClick={(e) => {
                                                            e.preventDefault();
                                                            handleCommentEdit(post.id, c.id)
                                                        }}><span>Edit</span></a>}



                                    </span></h6>

                                                <p className="mb-1">{c.content}</p>

                                                {c.likes.length > 0 && <p className="mb-0"> {c.likes.length} likes

                                                </p>}

                                                <div className="d-flex">

                                                    {!c.likes.find(x => x.id === currentUser.id) &&
                                                    <a href="" className="text-danger mr-2" onClick={(e) => {
                                                        e.preventDefault();
                                                        handleCommentLike(post.id, c.id)
                                                    }}><span>Like</span></a>
                                                    }

                                                    {c.likes.find(x => x.id === currentUser.id) &&
                                                    <a href="" className="text-dark mr-2" onClick={(e) => {
                                                        e.preventDefault();
                                                        handleCommentUnlike(post.id, c.id)
                                                    }}><span>Unlike</span></a>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>)
                            })}

                        </div>

                    </div>
                </div>
            </div>
        </div>
    });


    return (
        <Page>
            <Container className={"white-background"}>
                <br/>
                {postsList}
            </Container>
        </Page>
    )
}




