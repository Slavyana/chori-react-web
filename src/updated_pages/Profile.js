import './nicepage/Profile.css'
import './nicepage/nicepage.css'
import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {
    getBusiness,
    getBusinessEmployees,
    getBusinessPosts,
    getBusinessProducts,
    getBusinessServices
} from "../actions/business";
import {getEmployee} from "../actions/employee";
import {Link, Redirect} from "react-router-dom";
import {Button} from "reactstrap";
import {CustomModal, Page} from "../components/components";
import photo from "../static/images/photo-icon.png";
import {Container} from "react-bootstrap";
import User2 from "./img/user2.jpg";
import Rating from "@material-ui/lab/Rating";

export function NewProfile(props) {

    let useScript = url => {
        useEffect(() => {
            const script = document.createElement('script');
            script.src = url;
            script.async = true;
            document.body.appendChild(script);
        }, [url]);
    }

    const username = props.match.params.username;
    console.log(username)

    const {user: currentUser} = useSelector((state) => state.auth);
    const [business, setBusiness] = useState({});
    const [employee, setEmployee] = useState({id: 0, roles: []});
    const [services, setServices] = useState([]);
    const [employees, setEmployees] = useState([]);
    const [posts, setPosts] = useState([]);
    const [reviews, setReviews] = useState([]);
    const [products, setProducts] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [selectedPost, setSelectedPost] = useState(null);


    const [commentContent, setCommentContent] = useState([]);

    let handleLike = (id) => {
        console.log(id);
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + id + '/like', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify(currentUser),
        }).then(() => props.history.push('/profile'));
    }

    let handleUnlike = (id) => {
        console.log(id);
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + id + '/unlike/' + currentUser.id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then(() => props.history.push('/profile'));
    }


    let handleCommentLike = (postId, commentId) => {
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + postId + '/' + commentId + '/like', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify(currentUser),
        }).then(() => props.history.push('/profile'));
    }

    let handleCommentUnlike = (postId, commentId) => {
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        fetch('http://localhost:8080/api/posts/' + postId + '/' + commentId + '/unlike/' + currentUser.id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

        }).then(() => props.history.push('/profile'));
    }


    let handleComment = (id) => {
        console.log(id);
        console.log(JSON.stringify(currentUser));
        currentUser["roles"] = null;

        let comment = {
            author: currentUser,
            content: commentContent
        }

        fetch('http://localhost:8080/api/posts/' + id + '/comment/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify(comment),

        }).then(() => props.history.push('/profile'));
    }


    useScript('jquery.js');
    useScript('nicepage.js');


    useEffect(() => {

        if (!username) {
            try {
                getBusiness(currentUser.employee.business.id)
                    .then((response) => {
                        setBusiness(response);
                    });
            } catch (e) {
                props.history.push("/profile")
            }

        } else {
            fetch('http://localhost:8080/api/businesses?search=' + username)
                .then(response => response.json())
                .then(data => {
                    console.log(data)
                    setBusiness(data[0])
                });
        }
    }, []);


    useEffect(() => {

        if (currentUser.employee) {
            getEmployee(currentUser.employee.id)
                .then((response) => {
                    setEmployee(response);
                });
        }

        getBusinessServices(business.id)
            .then((response) => {
                setServices(response);
                console.log(response)
            });

        getBusinessEmployees(business.id)
            .then((response) => {
                setEmployees(response);
            });

        getBusinessPosts(business.id)
            .then((response) => {
                setPosts(response);
            });

        getBusinessProducts(business.id)
            .then((response) => {
                setProducts(response);
            });

        fetch('http://localhost:8080/api/businesses/' + business.id + '/reviews')
            .then(response => response.json())
            .then(data => {
                setReviews(data)
            });

    }, [business]);

    const reviewList = reviews.map(r => {
        return <div className="comment-part">
            <div className="comment">
                {r.isAnonymous &&
                <h4>Anonymous</h4>}

                {!r.isAnonymous &&
                <h4>{r.author.username}</h4>}

                <Rating name="read-only" value={r.rating} readOnly/>

                <p>{r.content}</p>
                <a href="" onClick={(e) => {
                    e.preventDefault();
                    alert("Please send an email with a description of the comment to sc17sdc@leeds.ac.uk for a review.")
                }}>Report</a>
                <hr/>

            </div>
            <div style={{clear: 'both'}}></div>

        </div>
    })


    if (!currentUser) {
        return <Redirect to="/"/>;
    }

    const serviceList = services.map(service => {
        return <tr style={{height: "75px"}}>
            <td className="u-border-1 u-border-grey-40 u-border-no-left u-border-no-right u-table-cell"> {service.name}
            </td>
            <td className="u-border-1 u-border-grey-40 u-border-no-left u-border-no-right u-table-cell">{service.description}</td>
            <td className="u-border-1 u-border-grey-40 u-border-no-left u-border-no-right u-table-cell">{service.price} {service.currency}</td>
            <td className="u-border-1 u-border-grey-40 u-border-no-left u-border-no-right u-table-cell">
                <button onClick={() => props.history.push("/" + business.id + "/appointments/add/" + service.id)}>Book
                    an appointment
                </button>
            </td>
            <td>{currentUser.employee && <Button tag={Link} to={"/profile/services/" + service.id} id={service.id}>Edit</Button>}</td>
        </tr>
    });

    const employeeList = employees.map(employee => {
        return <div className="u-align-left u-container-style u-list-item u-repeater-item">
            <div
                className="u-container-layout u-similar-container u-valign-top u-container-layout-2">
                <div alt="" className="u-image u-image-circle u-image-1"
                     data-image-width="881" data-image-height="1080">
                    {employee.profileImage &&
                    <img className="u-image u-image-circle u-image-1"
                         data-image-width="881" data-image-height="1080"
                         src={employee.profileImage && employee.profileImage.url}/>}</div>
                <h5 className="u-text u-text-1">{employee.firstName} {employee.lastName}</h5>
                {/*<p className="u-text u-text-2">@{employee.username}</p>*/}
                <p className="u-text u-text-2">{currentUser.employee && (employee.id === currentUser.employee.id || currentUser.roles && currentUser.roles.includes("ROLE_MANAGER")) &&
                <Button tag={Link} to={"/profile/employees/" + employee.id} id={employee.id}>Edit</Button>}
                </p>

            </div>

        </div>
    });

    let handlePostClick = (post) => {
        console.log(post)
        setSelectedPost(<div className="row">
            <div className=" offset-sm-3">
                <div className="post-block">
                    <div className="d-flex justify-content-between">
                        <div className="d-flex mb-3">
                            <div className="mr-2">
                            </div>
                            <div>
                                <h5 className="mb-0"><a href="#!" className="text-dark">{post.business.username}</a>
                                </h5>
                            </div>
                        </div>
                        <div className="post-block__user-options">
                            <a href="#!" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                <i className="fa fa-ellipsis-v" aria-hidden="true"></i>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right"
                                 aria-labelledby="triggerId">
                                <a className="dropdown-item text-dark" href="#!"><i
                                    className="fa fa-pencil mr-1"></i>Edit</a>
                                <a className="dropdown-item text-danger" href="#!"><i
                                    className="fa fa-trash mr-1"></i>Delete</a>
                            </div>
                        </div>
                    </div>
                    <div className="post-block__content mb-2">
                        <p>{post.description}</p>
                        <img src={post.image.url} alt="Content img"/>
                    </div>
                    <div className="mb-3">
                        <div className="d-flex justify-content-between mb-2">
                            <div className="d-flex">
                                <a href="#!" className="text-danger mr-2"><span>Like</span></a>
                                <a href="#!" className="text-dark mr-2"><span>Unlike</span></a>
                            </div>
                        </div>
                        {post.likes.length > 0 && <p className="mb-0">Liked by <a href="#!"
                                                                                  className="text-muted font-weight-bold">{post.likes[0].username}</a> &
                            <a href="#!" className="text-muted font-weight-bold"> {post.likes.length - 1} other</a>
                        </p>}

                    </div>
                    <hr/>
                    <div className="post-block__comments">
                        <div className="input-group mb-3">
                            <input type="text" className="form-control"
                                   placeholder="Add your comment"/>
                            <div className="input-group-append">
                                <button className="btn btn-primary" type="button"
                                        id="button-addon2"><i className="fa fa-paper-plane"></i>
                                </button>
                            </div>
                        </div>

                        {/* scroll */}

                        <div className="comment-view-box mb-3">
                            <div className="d-flex mb-2">
                                <img src={User2} alt="User img"
                                     className="author-img author-img--small mr-2"/>
                                <div>
                                    <h6 className="mb-1"><a href="#!" className="text-dark">John
                                        doe</a></h6>
                                    <p className="mb-1">Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit.</p>
                                    <div className="d-flex">
                                        <a href="#!" className="text-dark mr-2"><span><i
                                            className="fa fa-heart-o"></i></span></a>
                                        <a href="#!"
                                           className="text-dark mr-2"><span>Like</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr/>
                        <a href="#!" className="text-dark">View More comments <span
                            className="font-weight-bold">({post.comments.length})</span></a>
                    </div>
                </div>
            </div>
        </div>)
        setShowModal(true)
    }

    const postsList = posts.map(post => {
        return <div className="u-effect-fade u-gallery-item" onClick={() => handlePostClick(post)}>
            <div className="u-back-slide">
                {post.image && <img className="u-back-image u-expanded" src={post.image.url}/>}
            </div>
            <div className="u-over-slide u-shading u-over-slide-1">
                <h3 className="u-gallery-heading">{post.business.username}</h3>
                <p className="u-gallery-text">{post.description}</p>
            </div>
        </div>
    });

    const productsList = products.map(product => {
        return <div
            className="u-align-center u-container-style u-products-item u-repeater-item u-white u-repeater-item-5">
            <div
                className="u-container-layout u-similar-container u-container-layout-8">

                {product.image &&
                <img alt="" className="u-expanded-width u-image u-image-default u-product-control u-image-5"
                     src={product.image.url}/>}

                {!product.image &&
                <img alt="" className="u-expanded-width u-image u-image-default u-product-control u-image-5"
                     src={photo}/>}


                <h4 className="u-product-control u-text u-text-9">
                    <a className="u-product-title-link"
                       href="#">
                        {product.name}
                    </a>
                </h4>
                <div
                    className="u-product-control u-product-price u-product-price-1">
                    {product.description}
                        <div className="u-price u-text-palette-2-base"
                             style={{fontSize: "1.25rem", fontWeight: "700"}}>{product.price} {product.currency}</div>
                    {currentUser.employee && (currentUser.roles && currentUser.roles.includes("ROLE_MANAGER")) && <Button tag={Link} to={"/products/" + product.id} id={product.id}>Edit</Button>}

                </div>

            </div>

        </div>
    });


    const handleCloseModal = () => setShowModal(false);

    return (
        <Page>


            <Container className={"white-background "}>
                <body data-home-page="Profile.html" data-home-page-title="Profile" className="u-body">

                <CustomModal handleClose={handleCloseModal} showModal={showModal}>
                    <div>
                        <div>
                            {selectedPost}
                        </div>
                    </div>
                </CustomModal>
                <section className="u-clearfix u-section-1" id="carousel_c6fe">
                    <div className="u-clearfix u-layout-wrap u-layout-wrap-1">
                        <div className="u-layout">
                            <div className="u-layout-row">
                                <div className="u-align-left u-container-style u-layout-cell u-size-30 u-layout-cell-1">
                                    <div
                                        className="u-container-layout u-valign-middle-lg u-valign-middle-xl u-container-layout-1">
                                        <h2 className="u-custom-font u-font-merriweather u-text u-text-1">{business.name} <br/> @{business.username}</h2>
                                        <div
                                            className="u-border-3 u-border-grey-dark-1 u-expanded-width u-line u-line-horizontal u-line-1"></div>
                                        <h6 className="u-custom-font u-font-merriweather u-text u-text-2">About us</h6>
                                        <p className="u-text u-text-3">{business.description}</p>

                                        <div
                                            className="u-border-3 u-border-grey-dark-1 u-expanded-width u-line u-line-horizontal u-line-2"></div>
                                        <p className="u-text u-text-12">{business.country}, {business.region}</p>
                                        <p className="u-text u-text-13">{business.address}</p>
                                        {(currentUser.client || (currentUser.employee && currentUser.employee.business.id === business.id)) &&
                                        <a href={business.id + "/appointments/add"}
                                           className="u-active-palette-2-dark-3 u-btn u-btn-round u-button-style u-hover-palette-2-dark-3 u-palette-3-dark-1 u-radius-50 u-btn-2">Make
                                            an appointment now!</a>}
                                         {currentUser.employee && (currentUser.roles && currentUser.roles.includes("ROLE_MANAGER")) &&
                                        <a href={"/profile/business/" + business.id}
                                           className="u-active-palette-2-dark-3 u-btn u-btn-round u-button-style u-hover-palette-2-dark-3 u-palette-5-light-1 u-radius-50 u-btn-3">Edit
                                            profile</a>}
                                    </div>
                                </div>
                                <div className="u-container-style u-image u-layout-cell u-size-30 "
                                     data-image-width="1000" data-image-height="1000">
                                    {business.profileImage &&
                                    <img className={"profileImage"} src={business.profileImage.url}/>}
                                    {!business.profileImage && <img src={photo}/>}

                                    <div className="u-container-layout u-container-layout-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                </section>
                <section className="u-align-left u-clearfix u-section-2" id="sec-8149">
                    <div className="u-clearfix u-sheet u-sheet-1">
                        <div className="u-expanded-width u-tab-links-align-justify u-tabs u-tabs-1">
                            <ul className="u-spacing-30 u-tab-list u-unstyled" role="tablist">
                                <li className="u-tab-item" role="presentation">
                                    <a className="active u-active-palette-3-dark-1 u-button-style u-tab-link u-tab-link-1"
                                       id="link-tab-081f" href="#tab-081f" role="tab" aria-controls="tab-081f"
                                       aria-selected="true">Our Team</a>
                                </li>
                                <li className="u-tab-item" role="presentation">
                                    <a className="u-active-palette-3-dark-1 u-button-style u-tab-link u-tab-link-2"
                                       id="link-tab-4d57" href="#tab-4d57" role="tab" aria-controls="tab-4d57"
                                       aria-selected="false">Services</a>
                                </li>
                                <li className="u-tab-item" role="presentation">
                                    <a className="u-active-palette-3-dark-1 u-button-style u-tab-link u-tab-link-3"
                                       id="link-tab-3a42" href="#tab-3a42" role="tab" aria-controls="tab-3a42"
                                       aria-selected="false">Products</a>
                                </li>
                                <li className="u-tab-item u-tab-item-4" role="presentation">
                                    <a className="u-active-palette-3-dark-1 u-button-style u-tab-link u-tab-link-4"
                                       id="tab-6219" href="#link-tab-6219" role="tab" aria-controls="link-tab-6219"
                                       aria-selected="false">Reviews</a>
                                </li>
                            </ul>
                            <div className="u-tab-content">
                                <div className="u-container-style u-tab-active u-tab-pane" id="tab-081f" role="tabpanel"
                                     aria-labelledby="link-tab-081f">
                                    <div className="u-container-layout u-container-layout-1">
                                        <div
                                            className="u-expanded-width-md u-expanded-width-xs u-list u-repeater u-list-1">

                                            {employeeList}

                                        </div>
                                        {currentUser.employee && currentUser.employee.business.id === business.id && currentUser.roles.includes("ROLE_MANAGER") &&
                                        <Button className="button-medium" tag={Link} to={"/employees/"}>Add a new
                                            employee</Button>}


                                    </div>
                                </div>
                                <div className="u-align-left u-container-style u-tab-pane" id="tab-4d57" role="tabpanel"
                                     aria-labelledby="link-tab-4d57">
                                    <div className="u-container-layout u-container-layout-6">
                                        <div className="u-expanded-width u-table u-table-responsive u-table-1">
                                            <table className="u-table-entity">
                                                <colgroup>
                                                    <col width="25%"/>
                                                    <col width="25%"/>
                                                    <col width="25%"/>
                                                    <col width="25%"/>
                                                </colgroup>
                                                <thead className="u-grey-50 u-table-header u-table-header-1">
                                                <tr style={{height: "21px"}}>
                                                    <th className="u-border-1 u-border-grey-50 u-table-cell">Service</th>
                                                    <th className="u-border-1 u-border-grey-50 u-table-cell">Description</th>
                                                    <th className="u-border-1 u-border-grey-50 u-table-cell">Price</th>
                                                    <th className="u-border-1 u-border-grey-50 u-table-cell">Book</th>
                                                </tr>
                                                </thead>
                                                <tbody className="u-table-body">
                                                {serviceList}
                                                </tbody>
                                            </table>
                                        </div>
                                        {currentUser.employee && currentUser.employee.business.id === business.id &&
                                        <Button className="button-medium" tag={Link} to={"/services/"}>Add a new
                                            service</Button>}

                                    </div>
                                </div>
                                <div className="u-container-style u-tab-pane" id="tab-3a42" role="tabpanel"
                                     aria-labelledby="link-tab-3a42">
                                    <div className="u-container-layout u-container-layout-7">
                                        <div className="u-expanded-width u-products u-products-1">
                                            <div className="u-repeater u-repeater-2">

                                                {productsList}

                                            </div>
                                            {currentUser.employee && currentUser.employee.business.id === business.id && currentUser.roles.includes("ROLE_MANAGER") &&
                                            <Button className="button-medium" tag={Link} to={"/products/"}>Add a new
                                                product</Button>}

                                        </div>

                                    </div>
                                </div>
                                <div className="u-container-style u-tab-pane" id="link-tab-6219" role="tabpanel"
                                     aria-labelledby="tab-6219">
                                    <div className="u-container-layout u-container-layout-11">
                                        <div className="">
                                            <div className="rating-par scroll-view-box">

                                                <div className="reviews"><h1>Reviews</h1>
                                                    {currentUser.client && <Button className="button-medium" tag={Link}
                                                             to={"/" + business.id + "/reviews/add/"}>Add a
                                                        review</Button>}

                                                </div>
                                                {reviewList}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="u-align-center u-clearfix u-grey-10 u-section-3" id="sec-b9ec">
                    <div className="u-clearfix u-sheet u-sheet-1">
                        <h2 className="u-text u-text-1">Gallery</h2>
                        <div
                            className="u-expanded-width u-gallery u-layout-grid u-show-text-on-hover u-gallery-1">
                            <div className="u-gallery-inner u-gallery-inner-1">
                                {postsList}
                            </div>
                        </div>
                        <br/>
                        <br/>
                        {currentUser.employee && currentUser.employee.business.id === business.id &&
                        <Button className="button-medium" tag={Link} to={"/posts/"}>Add a new post</Button>}

                    </div>
                </section>


                </body>
            </Container>
        </Page>
    );


}

