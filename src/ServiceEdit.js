import React, { useEffect, useState} from 'react';
import { Container, Form, FormGroup, Input, Label } from 'reactstrap';
import {Page} from "./components/components";

export function ServiceEdit(props) {

    let emptyService = {
        name: '',
        description: '',
        duration: '',
        price: ''
    };

    console.log(props)

    const [isLoaded, setIsLoaded] = useState(false);
    const id = props.match.params.id;
    const [service, setService] = useState(emptyService);
    const [error, setError] = useState(null);

    let handleCancel = (event) => {
        props.history.push('/profile');
    }



    useEffect(() => {
        fetch(`http://localhost:8080/api/services/${id}`)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setService(result);
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    const title = <h2>{service.id ? 'Edit Service' : 'Add Service'}</h2>;


    let handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let changedService = {...service};
        changedService[name] = value;
        setService(changedService);
    }

    let handleSubmit = (event) => {
        event.preventDefault();
        fetch('http://localhost:8080/api/services' + (service.id ? '/' + id : ''), {
            method: (service.id) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(service),
        }).then(() => props.history.push('/profile'));
    }

    return (
<Page>
            <Container className={"white-background padding"}>
                <Form className={"new-form"} onSubmit={handleSubmit}>
                {title}
                    <FormGroup>
                        <Label for="name">Name</Label>
                        <Input type="text" name="name" id="name" value={service.name || ''}
                               onChange={handleChange} autoComplete="name"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="description">Description</Label>
                        <Input type="description" name="description" id="description" value={service.description || ''}
                               onChange={handleChange} autoComplete="description"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="price">Price</Label>
                        <Input type="price" name="price" id="price" value={service.price || ''}
                               onChange={handleChange} autoComplete="price"/>
                    </FormGroup>


                    <div>
                        <button className={"button-primary"} type={"submit"}>Save</button>
                        <button className={"button-secondary"} onClick={handleCancel}>Cancel</button>
                        <br/>
                    </div>
                </Form>

            </Container>
        </Page>);
}